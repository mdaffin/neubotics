use std::sync::{Arc, Mutex};
use wasm::{self, ExportInst, ExternVal, Interpreter, Registry};
use wast::{
    parser::{self, ParseBuffer},
    Wast, WastDirective,
};

use std::{fs::read_to_string, path::Path};

pub fn run(file: impl AsRef<Path>) {
    let file = file.as_ref();

    let wast = &read_to_string(file).expect("could not read test file");
    let buf = ParseBuffer::new(&wast).expect("could not create parser");
    let tests = parser::parse::<Wast>(&buf).expect("could not parse the test file");

    let mut registry = Registry::default();
    let mut interpreter = Interpreter::default();
    let mut last_name = "".to_string();

    init_spectest(&mut interpreter, &mut registry);

    for directive in tests.directives {
        match directive {
            WastDirective::Module(mut module) => {
                println!(
                    "\n=== Loading module on line {}",
                    line_no(&module.span, &wast)
                );

                last_name = module.id.map(|n| n.name().to_string()).unwrap_or_default();
                let wasm = module.encode().expect("could not encode wat module");
                let module = wasm::Module::from_reader(&mut &wasm[..]).expect("Invalid module");

                let imports = module
                    .imports
                    .iter()
                    .map(|import| {
                        registry
                            .get(import.module.as_str())
                            .unwrap()
                            .export(&import.name)
                            .unwrap()
                    })
                    .collect::<Vec<_>>();

                let module_inst = interpreter.load_module(&module, &imports);
                if module.start.is_some() {
                    interpreter.advance(1000);
                    interpreter
                        .results()
                        .expect("interpreter is still running")
                        .expect("a trap happen");
                }
                registry.add_module(last_name.clone(), module_inst.clone());
            }
            WastDirective::QuoteModule { span, source } => {
                let (_, _) = (span, source);
                dbg!("quote module");
            }
            WastDirective::AssertMalformed {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert malformed");
            }
            WastDirective::AssertInvalid {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert invalid");
            }
            WastDirective::Register {
                span: _,
                name,
                module,
            } => {
                let module_name = if let Some(module) = module {
                    module.name()
                } else {
                    &last_name
                };
                let module_inst = registry.get(module_name).unwrap();
                registry.add_module(name, module_inst);
                println!("\n=== Registered '{}' as '{}'", module_name, name);
            }
            WastDirective::Invoke(invoke) => {
                invoke_fn(&last_name, &registry, &mut interpreter, invoke).expect("found a trap");
            }
            WastDirective::AssertTrap {
                span,
                exec,
                message,
            } => {
                println!(
                    "\n=== Running assert trap on line {}",
                    line_no(&span, &wast)
                );
                let actual = dbg!(exec_fn(&last_name, exec, &registry, &mut interpreter))
                    .err()
                    .expect("no trap happened");
                dbg!(actual, message);
            }
            WastDirective::AssertReturn {
                span,
                exec,
                results,
            } => {
                println!(
                    "\n=== Running assert return on line {}",
                    line_no(&span, &wast)
                );

                let actual =
                    exec_fn(&last_name, exec, &registry, &mut interpreter).expect("found a trap");
                assert_eq!(actual.len(), results.len());
                assert_result(&results, &actual);
            }
            WastDirective::AssertExhaustion {
                span,
                call,
                message,
            } => {
                let (_, _, _) = (span, call, message);
                dbg!("assert exhaustion");
            }
            WastDirective::AssertUnlinkable {
                span,
                module,
                message,
            } => {
                let (_, _, _) = (span, module, message);
                dbg!("assert unlinkable");
            }
        }
    }
}

fn exec_fn<'i>(
    last_name: &str,
    exec: wast::WastExecute,
    registry: &Registry,
    interpreter: &'i mut Interpreter,
) -> Result<Vec<wasm::Value>, wasm::Trap> {
    match exec {
        wast::WastExecute::Invoke(invoke) => {
            let module_name = invoke.module.map(|id| id.name()).unwrap_or(last_name);
            Ok(invoke_fn(module_name, registry, interpreter, invoke)?.to_vec())
        }
        wast::WastExecute::Module(module) => {
            dbg!("invoke module", module.id, module.name,);
            match module.kind {
                wast::ModuleKind::Binary(_) => todo!("bin module not yet added"),
                wast::ModuleKind::Text(_) => unimplemented!("text modules not supported"),
            }
        }
        wast::WastExecute::Get { module, global } => {
            let module_name = module.map(|id| id.name()).unwrap_or(last_name);
            let module = registry.get(module_name).unwrap();
            Ok(vec![
                match module.export(global).unwrap() {
                    ExternVal::Global(addr) => interpreter.global(addr),
                    e => panic!("export is not a global: {:?}", e),
                }
                .value,
            ])
        }
    }
}

fn invoke_fn<'i>(
    module_name: &str,
    modules: &Registry,
    interpreter: &'i mut Interpreter,
    invoke: wast::WastInvoke,
) -> Result<&'i [wasm::Value], wasm::Trap> {
    let parameters = invoke
        .args
        .iter()
        .map(expression_to_value)
        .collect::<Vec<_>>();
    dbg!(&invoke.args, &parameters);
    let module = modules.get(module_name);
    interpreter
        .call(module.unwrap(), invoke.name, &parameters)
        .unwrap();
    interpreter.advance(5_000_000);
    assert!(
        !interpreter.is_running(),
        "interpreter is still running with test",
    );
    interpreter.results().expect("function not yet complete")
}

fn assert_result(results: &[wast::AssertExpression], actual: &[wasm::Value]) {
    for (expected, actual) in results.iter().zip(actual.iter()) {
        match expected {
            wast::AssertExpression::I32(i) => {
                assert_eq!(actual.to_i32(), Some(*i))
            }
            wast::AssertExpression::I64(i) => {
                assert_eq!(actual.to_i64(), Some(*i))
            }
            wast::AssertExpression::F32(i) => {
                assert_eq!(actual.to_f32().map(f32::to_bits), Some(to_inner(i).bits))
            }
            wast::AssertExpression::F64(i) => {
                assert_eq!(actual.to_f64().map(f64::to_bits), Some(to_inner(i).bits))
            }
            wast::AssertExpression::V128(_) => unimplemented!("v128"),
            wast::AssertExpression::RefNull(_) => unimplemented!("refnull"),
            wast::AssertExpression::RefExtern(_) => unimplemented!("refextern"),
            wast::AssertExpression::RefFunc(_) => unimplemented!("reffunc"),
            wast::AssertExpression::LegacyArithmeticNaN => match actual {
                wasm::Value::F32(f) => {
                    let mask = 0x7f80_0000;
                    assert_eq!(f.to_bits() & mask, mask);
                }

                wasm::Value::F64(f) => {
                    let mask = 0x7ff0_0000_0000_0000;
                    assert_eq!(f.to_bits() & mask, mask,)
                }

                v => panic!("expected nan got {:?}", v),
            },
            wast::AssertExpression::LegacyCanonicalNaN => match actual {
                wasm::Value::F32(f) => {
                    let mask = 0x7f80_0000;
                    assert_eq!(f.to_bits() & mask, mask);
                }

                wasm::Value::F64(f) => {
                    let mask = 0x7ff0_0000_0000_0000;
                    assert_eq!(f.to_bits() & mask, mask,)
                }
                v => panic!("expected nan got {:?}", v),
            },
        }
    }
}

fn init_spectest(interpreter: &mut Interpreter, registry: &mut Registry) {
    use wasm::types::{FuncType, Mut, ResultType, ValType::*};
    let mut exports = Vec::new();

    exports.push(wasm::ExportInst {
        name: "table".to_string(),
        value: wasm::ExternVal::Table(interpreter.alloc_table(wasm::TableInst {
            elem: vec![None; 10].into_boxed_slice(),
            max: Some(20),
        })),
    });

    exports.push(wasm::ExportInst {
        name: "memory".to_string(),
        value: wasm::ExternVal::Mem(interpreter.alloc_mem(wasm::MemInst {
            data: vec![0; 65536],
            max: Some(2),
        })),
    });

    exports.extend(
        [
            ("print", vec![]),
            ("print_i32", vec![I32]),
            ("print_i32_f32", vec![I32, F32]),
            ("print_f32", vec![F32]),
            ("print_f64", vec![F64]),
            ("print_f64_f64", vec![F64, F64]),
        ]
        .iter()
        .map(|(name, params)| wasm::ExportInst {
            name: name.to_string(),
            value: wasm::ExternVal::Func(interpreter.alloc_func(wasm::FuncInst::Host {
                func_type: FuncType {
                    params: ResultType(params.clone().into_boxed_slice()),
                    results: ResultType(Box::new([])),
                },
                host_code: Arc::new(Mutex::new(move |_: &_, _: &mut _| Ok(()))),
            })),
        }),
    );

    exports.extend(
        [
            ("global_i32", wasm::Value::I32(666)),
            ("global_f32", wasm::Value::F32(666.0)),
            ("global_f64", wasm::Value::F64(666.0)),
        ]
        .iter()
        .map(|(name, value)| wasm::ExportInst {
            name: name.to_string(),
            value: wasm::ExternVal::Global(interpreter.alloc_global(wasm::GlobalInst {
                value: *value,
                mutable: Mut::Const,
            })),
        }),
    );

    let module_inst = Arc::new(wasm::ModuleInst {
        types: Box::new([]),
        func_addrs: Box::new([]),
        table_addrs: Box::new([]),
        mem_addrs: Box::new([]),
        global_addrs: Box::new([]),
        exports: exports.into_boxed_slice(),
    });

    registry.add_module("spectest".to_string(), module_inst);
}

fn expression_to_value(expression: &wast::Expression) -> wasm::Value {
    assert_eq!(expression.instrs.len(), 1);
    match &expression.instrs[0] {
        wast::Instruction::I32Const(v) => wasm::Value::I32(*v),
        wast::Instruction::I64Const(v) => wasm::Value::I64(*v),
        wast::Instruction::F32Const(v) => wasm::Value::F32(f32::from_bits(v.bits)),
        wast::Instruction::F64Const(v) => wasm::Value::F64(f64::from_bits(v.bits)),
        instruction => panic!("non-const instruction: {:?}", instruction),
    }
}

fn to_inner<T>(value: &wast::NanPattern<T>) -> &T {
    match value {
        wast::NanPattern::ArithmeticNan => unimplemented!(),
        wast::NanPattern::CanonicalNan => unimplemented!(),
        wast::NanPattern::Value(t) => t,
    }
}

fn line_no(span: &wast::Span, wast: &str) -> usize {
    let (line, _) = span.linecol_in(wast);
    line + 1
}
