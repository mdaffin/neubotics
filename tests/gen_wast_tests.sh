#!/bin/bash
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'

BASE_DIR=$(dirname "$(readlink -f "$0")")
BASE_URL=https://raw.githubusercontent.com/WebAssembly/spec/wg-1.0/test/core

download_wast() {
    test_dir="${BASE_DIR}/suite"
    mkdir -p "${test_dir}"
    curl -SsfL "${BASE_URL}/$1.wast" -o "${test_dir}/$1.wast"
    echo "#[test] fn $(echo $1 | tr - _ )_wast() { test_suite::run(\"tests/suite/$1.wast\"); }" >>"${BASE_DIR}/gened_tests.rs"
}

rm -rf "${BASE_DIR}/suite"
cat >"${BASE_DIR}/gened_tests.rs" <<EOF
mod test_suite;

EOF

download_wast address
download_wast align
download_wast binary-leb128
download_wast binary
download_wast block
download_wast br
download_wast br_if
download_wast br_table
download_wast break-drop
download_wast call
download_wast call_indirect
download_wast comments
download_wast const
download_wast conversions
download_wast custom
download_wast data
download_wast elem
download_wast endianness
download_wast exports
download_wast f32
download_wast f32_bitwise
download_wast f32_cmp
download_wast f64
download_wast f64_bitwise
download_wast f64_cmp
download_wast fac
download_wast float_exprs
download_wast float_literals
download_wast float_memory
download_wast float_misc
download_wast forward
download_wast func
download_wast func_ptrs
download_wast globals
download_wast i32
download_wast i64
download_wast if
download_wast imports
download_wast inline-module
download_wast int_exprs
download_wast int_literals
download_wast labels
download_wast left-to-right
download_wast linking
download_wast load
download_wast local_get
download_wast local_set
download_wast local_tee
download_wast loop
download_wast memory
download_wast memory_grow
download_wast memory_redundancy
download_wast memory_size
download_wast memory_trap
download_wast names
download_wast nop
download_wast return
download_wast select
download_wast skip-stack-guard-page
download_wast stack
download_wast start
download_wast store
download_wast switch
download_wast token
download_wast traps
download_wast type
download_wast unreachable
download_wast unreached-invalid
download_wast unwind
download_wast utf8-custom-section-id
download_wast utf8-import-field
download_wast utf8-import-module
download_wast utf8-invalid-encoding
