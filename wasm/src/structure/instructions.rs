use super::types::{Block, FloatType, IntType, ValType};
use crate::values::Value;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LoadOption {
    S8,
    U8,
    S16,
    U16,
    S32,
    U32,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum StoreOption {
    U8,
    U16,
    U32,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum UnOp {
    IUnOp(IUnOp),
    FUnOp(FUnOp),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BinOp {
    IBinOp(IBinOp),
    FBinOp(FBinOp),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum RelOp {
    IRelOp(IRelOp),
    FRelOp(FRelOp),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum CvtOp {
    Convert {
        from: IntType,
        to: FloatType,
        signed: bool,
    },

    Trunc {
        from: FloatType,
        to: IntType,
        signed: bool,
    },

    I32ReinterpretF32,
    I64ReinterpretF64,
    F32ReinterpretI32,
    F64ReinterpretI64,

    F32DemoteF64,
    F64PromoteF32,

    I32WrapI64,

    I64ExtendSI32,
    I64ExtendUI32,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum IUnOp {
    Clz,
    Ctz,
    Popcnt,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum IBinOp {
    Add,
    Sub,
    Mul,
    DivS,
    DivU,
    RemS,
    RemU,
    And,
    Or,
    Xor,
    Shl,
    ShrS,
    ShrU,
    Rotl,
    Rotr,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ITestOp {
    Eqz,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum IRelOp {
    Eq,
    Ne,
    LtS,
    LtU,
    GtS,
    GtU,
    LeS,
    LeU,
    GeS,
    GeU,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FUnOp {
    Abs,
    Neg,
    Sqrt,
    Ceil,
    Floor,
    Trunc,
    Nearest,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FBinOp {
    Add,
    Sub,
    Mul,
    Div,
    Min,
    Max,
    Copysign,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FRelOp {
    Eq,
    Ne,
    Lt,
    Gt,
    Le,
    Ge,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Instr {
    Unreachable,
    Nop,
    Block(Block),
    Loop(Block),
    If(Block),
    Else,
    End,
    Br(u32),
    BrIf(u32),
    BrTable(Box<[u32]>, u32),
    Return,
    Call(u32),
    CallIndirect(u32, u8),
    Drop,
    Select,
    GetLocal(u32),
    SetLocal(u32),
    TeeLocal(u32),
    GetGlobal(u32),
    SetGlobal(u32),
    Load {
        l_type: ValType,
        align: u32,
        offset: u32,
        options: Option<LoadOption>,
    },
    Store {
        l_type: ValType,
        align: u32,
        offset: u32,
        options: Option<StoreOption>,
    },
    CurrentMemory(u8),
    GrowMemory(u8),
    Const(Value),
    UnOp(ValType, UnOp),
    BinOp(ValType, BinOp),
    ITestOp(IntType, ITestOp),
    RelOp(ValType, RelOp),
    CvtOp(CvtOp),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ConstInstr {
    I32Const(i32),
    I64Const(i64),
    F32Const(u32),
    F64Const(u64),
    GetGlobal(u32),
    End,
}

impl LoadOption {
    pub fn bit_width(&self) -> u8 {
        use LoadOption::*;
        match self {
            S8 | U8 => 8,
            S16 | U16 => 16,
            S32 | U32 => 32,
        }
    }
}

impl StoreOption {
    pub fn bit_width(&self) -> u8 {
        use StoreOption::*;
        match self {
            U8 => 8,
            U16 => 16,
            U32 => 32,
        }
    }
}
