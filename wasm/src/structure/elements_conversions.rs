use super::instructions::*;
use super::modules::*;
use super::types::*;
use crate::values::Value;
use parity_wasm::elements;

impl From<elements::BlockType> for Block {
    fn from(src: elements::BlockType) -> Self {
        match src {
            elements::BlockType::Value(value) => Block::Value(value.into()),
            elements::BlockType::NoResult => Block::NoResult,
        }
    }
}

impl From<&elements::Type> for FuncType {
    fn from(src: &elements::Type) -> Self {
        match src {
            elements::Type::Function(function_type) => FuncType {
                params: ResultType::from(
                    function_type
                        .params()
                        .iter()
                        .map(From::from)
                        .collect::<Vec<_>>(),
                ),
                results: ResultType::from(
                    function_type
                        .results()
                        .iter()
                        .map(From::from)
                        .collect::<Vec<_>>(),
                ),
            },
        }
    }
}

impl From<&elements::ValueType> for ValType {
    fn from(src: &elements::ValueType) -> Self {
        (*src).into()
    }
}

impl From<elements::ValueType> for ValType {
    fn from(src: elements::ValueType) -> Self {
        match src {
            elements::ValueType::I32 => ValType::I32,
            elements::ValueType::I64 => ValType::I64,
            elements::ValueType::F32 => ValType::F32,
            elements::ValueType::F64 => ValType::F64,
        }
    }
}

impl From<elements::TableType> for TableType {
    fn from(src: elements::TableType) -> Self {
        TableType((*src.limits()).into(), src.elem_type().into())
    }
}

impl From<elements::MemoryType> for MemType {
    fn from(src: elements::MemoryType) -> Self {
        MemType((*src.limits()).into())
    }
}

impl From<elements::GlobalType> for GlobalType {
    fn from(src: elements::GlobalType) -> Self {
        GlobalType(
            match src.is_mutable() {
                true => Mut::Var,
                false => Mut::Const,
            },
            src.content_type().into(),
        )
    }
}

impl From<elements::ResizableLimits> for Limits {
    fn from(src: elements::ResizableLimits) -> Self {
        Limits {
            min: src.initial(),
            max: src.maximum(),
        }
    }
}

impl From<elements::TableElementType> for ElemType {
    fn from(src: elements::TableElementType) -> Self {
        match src {
            elements::TableElementType::AnyFunc => ElemType::FuncRef,
        }
    }
}

impl From<&parity_wasm::elements::Instruction> for ConstInstr {
    fn from(src: &parity_wasm::elements::Instruction) -> Self {
        match src {
            elements::Instruction::GetGlobal(a) => ConstInstr::GetGlobal(*a),
            elements::Instruction::I32Const(a) => ConstInstr::I32Const(*a),
            elements::Instruction::I64Const(a) => ConstInstr::I64Const(*a),
            elements::Instruction::F32Const(a) => ConstInstr::F32Const(*a),
            elements::Instruction::F64Const(a) => ConstInstr::F64Const(*a),
            elements::Instruction::End => ConstInstr::End,
            v => panic!("non-const instruction: {:?}", v),
        }
    }
}

impl From<parity_wasm::elements::Instruction> for ConstInstr {
    fn from(src: parity_wasm::elements::Instruction) -> Self {
        ConstInstr::from(&src)
    }
}

impl From<&parity_wasm::elements::Instruction> for Instr {
    fn from(src: &parity_wasm::elements::Instruction) -> Self {
        src.to_owned().into()
    }
}

impl From<&elements::Internal> for ExportDesc {
    fn from(src: &elements::Internal) -> Self {
        use elements::Internal;
        match src {
            &Internal::Function(v) => ExportDesc::Func(FuncIdx(v as usize)),
            &Internal::Table(v) => ExportDesc::Table(TableIdx(v as usize)),
            &Internal::Memory(v) => ExportDesc::Mem(MemIdx(v as usize)),
            &Internal::Global(v) => ExportDesc::Global(GlobalIdx(v as usize)),
        }
    }
}

impl From<&elements::External> for ImportDesc {
    fn from(src: &elements::External) -> Self {
        use elements::External;
        match src {
            &External::Function(v) => ImportDesc::Func(TypeIdx(v as usize)),
            &External::Table(v) => ImportDesc::Table(v.into()),
            &External::Memory(v) => ImportDesc::Mem(v.into()),
            &External::Global(v) => ImportDesc::Global(v.into()),
        }
    }
}

impl From<parity_wasm::elements::Instruction> for Instr {
    fn from(src: parity_wasm::elements::Instruction) -> Self {
        let load = |l_type, align, offset, options| Instr::Load {
            l_type,
            align,
            offset,
            options,
        };
        let store = |l_type, align, offset, options| Instr::Store {
            l_type,
            align,
            offset,
            options,
        };
        use elements::Instruction as EI;
        use LoadOption as LO;
        use StoreOption as SO;
        match src {
            EI::Unreachable => Instr::Unreachable,
            EI::Nop => Instr::Nop,
            EI::Block(block_type) => Instr::Block(block_type.into()),
            EI::Loop(block_type) => Instr::Loop(block_type.into()),
            EI::If(block_type) => Instr::If(block_type.into()),
            EI::Else => Instr::Else,
            EI::End => Instr::End,
            EI::Br(a) => Instr::Br(a),
            EI::BrIf(a) => Instr::BrIf(a),
            EI::BrTable(t) => Instr::BrTable(t.table, t.default),
            EI::Return => Instr::Return,
            EI::Call(a) => Instr::Call(a),
            EI::CallIndirect(a, b) => Instr::CallIndirect(a, b),
            EI::Drop => Instr::Drop,
            EI::Select => Instr::Select,
            EI::GetLocal(a) => Instr::GetLocal(a),
            EI::SetLocal(a) => Instr::SetLocal(a),
            EI::TeeLocal(a) => Instr::TeeLocal(a),
            EI::GetGlobal(a) => Instr::GetGlobal(a),
            EI::SetGlobal(a) => Instr::SetGlobal(a),
            EI::I32Load(a, o) => load(ValType::I32, a, o, None),
            EI::I64Load(a, o) => load(ValType::I64, a, o, None),
            EI::F32Load(a, o) => load(ValType::F32, a, o, None),
            EI::F64Load(a, o) => load(ValType::F64, a, o, None),
            EI::I32Load8S(a, o) => load(ValType::I32, a, o, Some(LO::S8)),
            EI::I32Load8U(a, o) => load(ValType::I32, a, o, Some(LO::U8)),
            EI::I32Load16S(a, o) => load(ValType::I32, a, o, Some(LO::S16)),
            EI::I32Load16U(a, o) => load(ValType::I32, a, o, Some(LO::U16)),
            EI::I64Load8S(a, o) => load(ValType::I64, a, o, Some(LO::S8)),
            EI::I64Load8U(a, o) => load(ValType::I64, a, o, Some(LO::U8)),
            EI::I64Load16S(a, o) => load(ValType::I64, a, o, Some(LO::S16)),
            EI::I64Load16U(a, o) => load(ValType::I64, a, o, Some(LO::U16)),
            EI::I64Load32S(a, o) => load(ValType::I64, a, o, Some(LO::S32)),
            EI::I64Load32U(a, o) => load(ValType::I64, a, o, Some(LO::U32)),
            EI::I32Store(a, o) => store(ValType::I32, a, o, None),
            EI::I64Store(a, o) => store(ValType::I64, a, o, None),
            EI::F32Store(a, o) => store(ValType::F32, a, o, None),
            EI::F64Store(a, o) => store(ValType::F64, a, o, None),
            EI::I32Store8(a, o) => store(ValType::I32, a, o, Some(SO::U8)),
            EI::I32Store16(a, o) => store(ValType::I32, a, o, Some(SO::U16)),
            EI::I64Store8(a, o) => store(ValType::I64, a, o, Some(SO::U8)),
            EI::I64Store16(a, o) => store(ValType::I64, a, o, Some(SO::U16)),
            EI::I64Store32(a, o) => store(ValType::I64, a, o, Some(SO::U32)),
            EI::CurrentMemory(a) => Instr::CurrentMemory(a),
            EI::GrowMemory(a) => Instr::GrowMemory(a),
            EI::I32Const(v) => Instr::Const(Value::I32(v)),
            EI::I64Const(v) => Instr::Const(Value::I64(v)),
            EI::F32Const(v) => Instr::Const(Value::F32(f32::from_bits(v))),
            EI::F64Const(v) => Instr::Const(Value::F64(f64::from_bits(v))),
            EI::I32Eqz => Instr::ITestOp(IntType::I32, ITestOp::Eqz),
            EI::I32Eq => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::Eq)),
            EI::I32Ne => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::Ne)),
            EI::I32LtS => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::LtS)),
            EI::I32LtU => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::LtU)),
            EI::I32GtS => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::GtS)),
            EI::I32GtU => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::GtU)),
            EI::I32LeS => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::LeS)),
            EI::I32LeU => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::LeU)),
            EI::I32GeS => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::GeS)),
            EI::I32GeU => Instr::RelOp(ValType::I32, RelOp::IRelOp(IRelOp::GeU)),
            EI::I64Eqz => Instr::ITestOp(IntType::I64, ITestOp::Eqz),
            EI::I64Eq => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::Eq)),
            EI::I64Ne => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::Ne)),
            EI::I64LtS => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::LtS)),
            EI::I64LtU => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::LtU)),
            EI::I64GtS => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::GtS)),
            EI::I64GtU => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::GtU)),
            EI::I64LeS => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::LeS)),
            EI::I64LeU => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::LeU)),
            EI::I64GeS => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::GeS)),
            EI::I64GeU => Instr::RelOp(ValType::I64, RelOp::IRelOp(IRelOp::GeU)),
            EI::F32Eq => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Eq)),
            EI::F32Ne => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Ne)),
            EI::F32Lt => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Lt)),
            EI::F32Gt => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Gt)),
            EI::F32Le => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Le)),
            EI::F32Ge => Instr::RelOp(ValType::F32, RelOp::FRelOp(FRelOp::Ge)),
            EI::F64Eq => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Eq)),
            EI::F64Ne => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Ne)),
            EI::F64Lt => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Lt)),
            EI::F64Gt => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Gt)),
            EI::F64Le => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Le)),
            EI::F64Ge => Instr::RelOp(ValType::F64, RelOp::FRelOp(FRelOp::Ge)),
            EI::I32Clz => Instr::UnOp(ValType::I32, UnOp::IUnOp(IUnOp::Clz)),
            EI::I32Ctz => Instr::UnOp(ValType::I32, UnOp::IUnOp(IUnOp::Ctz)),
            EI::I32Popcnt => Instr::UnOp(ValType::I32, UnOp::IUnOp(IUnOp::Popcnt)),
            EI::I32Add => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Add)),
            EI::I32Sub => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Sub)),
            EI::I32Mul => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Mul)),
            EI::I32DivS => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::DivS)),
            EI::I32DivU => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::DivU)),
            EI::I32RemS => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::RemS)),
            EI::I32RemU => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::RemU)),
            EI::I32And => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::And)),
            EI::I32Or => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Or)),
            EI::I32Xor => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Xor)),
            EI::I32Shl => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Shl)),
            EI::I32ShrS => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::ShrS)),
            EI::I32ShrU => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::ShrU)),
            EI::I32Rotl => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Rotl)),
            EI::I32Rotr => Instr::BinOp(ValType::I32, BinOp::IBinOp(IBinOp::Rotr)),
            EI::I64Clz => Instr::UnOp(ValType::I64, UnOp::IUnOp(IUnOp::Clz)),
            EI::I64Ctz => Instr::UnOp(ValType::I64, UnOp::IUnOp(IUnOp::Ctz)),
            EI::I64Popcnt => Instr::UnOp(ValType::I64, UnOp::IUnOp(IUnOp::Popcnt)),
            EI::I64Add => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Add)),
            EI::I64Sub => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Sub)),
            EI::I64Mul => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Mul)),
            EI::I64DivS => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::DivS)),
            EI::I64DivU => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::DivU)),
            EI::I64RemS => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::RemS)),
            EI::I64RemU => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::RemU)),
            EI::I64And => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::And)),
            EI::I64Or => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Or)),
            EI::I64Xor => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Xor)),
            EI::I64Shl => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Shl)),
            EI::I64ShrS => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::ShrS)),
            EI::I64ShrU => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::ShrU)),
            EI::I64Rotl => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Rotl)),
            EI::I64Rotr => Instr::BinOp(ValType::I64, BinOp::IBinOp(IBinOp::Rotr)),
            EI::F32Abs => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Abs)),
            EI::F32Neg => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Neg)),
            EI::F32Sqrt => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Sqrt)),
            EI::F32Ceil => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Ceil)),
            EI::F32Floor => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Floor)),
            EI::F32Trunc => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Trunc)),
            EI::F32Nearest => Instr::UnOp(ValType::F32, UnOp::FUnOp(FUnOp::Nearest)),
            EI::F32Add => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Add)),
            EI::F32Sub => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Sub)),
            EI::F32Mul => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Mul)),
            EI::F32Div => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Div)),
            EI::F32Min => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Min)),
            EI::F32Max => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Max)),
            EI::F32Copysign => Instr::BinOp(ValType::F32, BinOp::FBinOp(FBinOp::Copysign)),
            EI::F64Abs => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Abs)),
            EI::F64Neg => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Neg)),
            EI::F64Sqrt => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Sqrt)),
            EI::F64Ceil => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Ceil)),
            EI::F64Floor => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Floor)),
            EI::F64Trunc => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Trunc)),
            EI::F64Nearest => Instr::UnOp(ValType::F64, UnOp::FUnOp(FUnOp::Nearest)),
            EI::F64Add => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Add)),
            EI::F64Sub => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Sub)),
            EI::F64Mul => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Mul)),
            EI::F64Div => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Div)),
            EI::F64Min => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Min)),
            EI::F64Max => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Max)),
            EI::F64Copysign => Instr::BinOp(ValType::F64, BinOp::FBinOp(FBinOp::Copysign)),
            EI::I32WrapI64 => Instr::CvtOp(CvtOp::I32WrapI64),
            EI::I32TruncSF32 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F32,
                to: IntType::I32,
                signed: true,
            }),
            EI::I32TruncUF32 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F32,
                to: IntType::I32,
                signed: false,
            }),
            EI::I32TruncSF64 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F64,
                to: IntType::I32,
                signed: true,
            }),
            EI::I32TruncUF64 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F64,
                to: IntType::I32,
                signed: false,
            }),
            EI::I64TruncSF32 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F32,
                to: IntType::I64,
                signed: true,
            }),
            EI::I64TruncUF32 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F32,
                to: IntType::I64,
                signed: false,
            }),
            EI::I64TruncSF64 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F64,
                to: IntType::I64,
                signed: true,
            }),
            EI::I64TruncUF64 => Instr::CvtOp(CvtOp::Trunc {
                from: FloatType::F64,
                to: IntType::I64,
                signed: false,
            }),
            EI::F32ConvertSI32 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I32,
                to: FloatType::F32,
                signed: true,
            }),
            EI::F32ConvertUI32 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I32,
                to: FloatType::F32,
                signed: false,
            }),
            EI::F32ConvertSI64 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I64,
                to: FloatType::F32,
                signed: true,
            }),
            EI::F32ConvertUI64 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I64,
                to: FloatType::F32,
                signed: false,
            }),
            EI::F64ConvertSI32 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I32,
                to: FloatType::F64,
                signed: true,
            }),
            EI::F64ConvertUI32 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I32,
                to: FloatType::F64,
                signed: false,
            }),
            EI::F64ConvertSI64 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I64,
                to: FloatType::F64,
                signed: true,
            }),
            EI::F64ConvertUI64 => Instr::CvtOp(CvtOp::Convert {
                from: IntType::I64,
                to: FloatType::F64,
                signed: false,
            }),
            EI::I64ExtendSI32 => Instr::CvtOp(CvtOp::I64ExtendSI32),
            EI::I64ExtendUI32 => Instr::CvtOp(CvtOp::I64ExtendUI32),
            EI::F32DemoteF64 => Instr::CvtOp(CvtOp::F32DemoteF64),
            EI::F64PromoteF32 => Instr::CvtOp(CvtOp::F64PromoteF32),
            EI::I32ReinterpretF32 => Instr::CvtOp(CvtOp::I32ReinterpretF32),
            EI::I64ReinterpretF64 => Instr::CvtOp(CvtOp::I64ReinterpretF64),
            EI::F32ReinterpretI32 => Instr::CvtOp(CvtOp::F32ReinterpretI32),
            EI::F64ReinterpretI64 => Instr::CvtOp(CvtOp::F64ReinterpretI64),
        }
    }
}
