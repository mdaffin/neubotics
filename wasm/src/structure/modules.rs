use crate::structure::instructions::{ConstInstr, Instr};
use crate::types;
use parity_wasm::elements::Deserialize;
use std::{fs, io, path::Path};
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct Module {
    pub types: Vec<types::FuncType>,
    pub funcs: Vec<Func>,
    pub tables: Vec<Table>,
    pub mems: Vec<Mem>,
    pub globals: Vec<Global>,
    pub elem: Vec<Elem>,
    pub data: Vec<Data>,
    pub start: Option<Start>,
    pub imports: Vec<Import>,
    pub exports: Vec<Export>,
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct TypeIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct FuncIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct TableIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct MemIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct GlobalIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct LocalIdx(pub usize);
#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub struct LabelIdx(pub usize);
#[derive(Debug, Default, PartialEq, PartialOrd, Clone, Copy)]
pub struct InstructionIndex(pub usize);

#[derive(Debug, Clone, PartialEq)]
pub struct Func {
    pub type_idx: TypeIdx,
    pub locals: Vec<types::ValType>,
    pub body: Expr,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr(pub Vec<Instr>);
#[derive(Debug, Clone, PartialEq)]
pub struct ConstExpr(pub Vec<ConstInstr>);

#[derive(Debug, Clone)]
pub struct Table {
    pub table_type: types::TableType,
}

#[derive(Debug, Clone)]
pub struct Mem {
    pub mem_type: types::MemType,
}

#[derive(Debug, Clone)]
pub struct Global {
    pub global_type: types::GlobalType,
    pub init: ConstExpr,
}

#[derive(Debug, Clone)]
pub struct Elem {
    pub table: TableIdx,
    pub offset: ConstExpr,
    pub init: Vec<FuncIdx>,
}

#[derive(Debug, Clone)]
pub struct Data {
    pub data: MemIdx,
    pub offset: ConstExpr,
    pub init: Vec<u8>,
}

#[derive(Debug, Clone)]
pub struct Start {
    pub func: FuncIdx,
}

#[derive(Debug, Clone)]
pub struct Export {
    pub name: String,
    pub desc: ExportDesc,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ExportDesc {
    Func(FuncIdx),
    Table(TableIdx),
    Mem(MemIdx),
    Global(GlobalIdx),
}

#[derive(Debug, Clone)]
pub struct Import {
    pub module: String,
    pub name: String,
    pub desc: ImportDesc,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ImportDesc {
    Func(TypeIdx),
    Table(types::TableType),
    Mem(types::MemType),
    Global(types::GlobalType),
}

////

impl InstructionIndex {
    pub fn increment(&mut self) {
        self.0 += 1;
    }
}

impl Func {
    pub fn instruction(&self, index: InstructionIndex) -> Instr {
        self.body
            .0
            .get(index.0)
            .expect("no more instructions")
            .clone()
    }
}

impl Module {
    pub fn from_file(path: impl AsRef<Path>) -> Result<Result<Module, ModuleError>, io::Error> {
        Ok(Self::from_reader(&mut fs::File::open(path)?))
    }

    pub fn from_reader<R: io::Read>(reader: &mut R) -> Result<Module, ModuleError> {
        let parity_module = parity_wasm::elements::Module::deserialize(reader).unwrap();

        if parity_module.version() != 1 {
            return Err(ModuleError::UnsupportedVersion(parity_module.version()));
        }

        let types = if let Some(types) = parity_module.type_section() {
            types.types().iter().map(From::from).collect()
        } else {
            Vec::new()
        };

        let funcs = match (
            parity_module.function_section(),
            parity_module.code_section(),
        ) {
            (Some(functions), Some(body)) => functions
                .entries()
                .iter()
                .zip(body.bodies().iter())
                .map(|(function, body)| {
                    let type_idx = TypeIdx(function.type_ref() as usize);
                    assert!(
                        type_idx.0 < types.len(),
                        "type_idx is out of range got {}, types length {}",
                        type_idx.0,
                        types.len()
                    );
                    let locals = body
                        .locals()
                        .iter()
                        .flat_map(|local| {
                            (0..local.count()).map(move |_| local.value_type().into())
                        })
                        .collect();
                    let body = Expr(body.code().elements().iter().map(From::from).collect());
                    Func {
                        type_idx,
                        locals,
                        body,
                    }
                })
                .collect(),
            (None, None) => Vec::new(),
            (Some(function), None) if function.entries().len() == 0 => Vec::new(),
            (None, Some(body)) if body.bodies().len() == 0 => Vec::new(),
            _ => unimplemented!(),
        };

        let tables = if let Some(table_section) = parity_module.table_section() {
            table_section
                .entries()
                .iter()
                .map(|table| {
                    let limits = table.limits();
                    let min = limits.initial();
                    let max = limits.maximum();
                    Table {
                        table_type: types::TableType(
                            types::Limits { min, max },
                            types::ElemType::FuncRef,
                        ),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        let elem = if let Some(element_section) = parity_module.elements_section() {
            element_section
                .entries()
                .iter()
                .map(|elem| Elem {
                    table: TableIdx(elem.index() as usize),
                    offset: ConstExpr(
                        elem.offset()
                            .as_ref()
                            .map(|offset| offset.code().iter().map(From::from).collect())
                            .unwrap(),
                    ),
                    init: elem
                        .members()
                        .iter()
                        .map(|&m| FuncIdx(m as usize))
                        .collect(),
                })
                .collect()
        } else {
            Vec::new()
        };

        let mems = if let Some(memory_section) = parity_module.memory_section() {
            memory_section
                .entries()
                .iter()
                .map(|memory| {
                    let limits = memory.limits();
                    let min = limits.initial();
                    let max = limits.maximum();
                    Mem {
                        mem_type: types::MemType(types::Limits { min, max }),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        let data = if let Some(data_section) = parity_module.data_section() {
            data_section
                .entries()
                .iter()
                .map(|data| Data {
                    data: MemIdx(data.index() as usize),
                    offset: ConstExpr(
                        data.offset()
                            .as_ref()
                            .map(|offset| offset.code().iter().map(From::from).collect())
                            .unwrap(),
                    ),
                    init: data.value().to_vec(),
                })
                .collect()
        } else {
            Vec::new()
        };

        let globals = if let Some(global_section) = parity_module.global_section() {
            global_section
                .entries()
                .iter()
                .map(|global| {
                    let global_type = global.global_type();
                    let mutable = match global_type.is_mutable() {
                        true => types::Mut::Var,
                        false => types::Mut::Const,
                    };
                    Global {
                        global_type: types::GlobalType(mutable, global_type.content_type().into()),
                        init: ConstExpr(global.init_expr().code().iter().map(From::from).collect()),
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        let start = parity_module.start_section().map(|index| Start {
            func: FuncIdx(index as usize),
        });

        let imports = parity_module
            .import_section()
            .map(|import_section| {
                import_section
                    .entries()
                    .iter()
                    .map(|import| Import {
                        module: import.module().to_string(),
                        name: import.field().to_string(),
                        desc: import.external().into(),
                    })
                    .collect()
            })
            .unwrap_or_default();

        let exports = parity_module
            .export_section()
            .map(|export_section| {
                export_section
                    .entries()
                    .iter()
                    .map(|export| Export {
                        name: export.field().to_string(),
                        desc: export.internal().into(),
                    })
                    .collect()
            })
            .unwrap_or_default();

        Ok(Module {
            types,
            funcs,
            tables,
            mems,
            globals,
            elem,
            data,
            start,
            imports,
            exports,
        })
    }

    pub(crate) fn types(&self, idx: TypeIdx) -> Option<&types::FuncType> {
        self.types.get(idx.0)
    }

    pub(crate) fn extern_types(&self) -> impl Iterator<Item = types::ExternType> + '_ {
        self.imports.iter().map(move |import| match import.desc {
            ImportDesc::Func(idx) => types::ExternType::Func(self.types(idx).unwrap().clone()),
            ImportDesc::Table(t) => types::ExternType::Table(t),
            ImportDesc::Mem(t) => types::ExternType::Mem(t),
            ImportDesc::Global(t) => types::ExternType::Global(t),
        })
    }
}

#[derive(Error, Debug)]
pub enum ModuleError {
    #[error("unsupported version got verion '{0}' expected version '1'")]
    UnsupportedVersion(u32),
    #[error("constant out of range")]
    ConstantOutOfRange,
}
