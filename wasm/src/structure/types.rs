#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ValType {
    I32,
    I64,
    F32,
    F64,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum FloatType {
    F32,
    F64,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum IntType {
    I32,
    I64,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ResultType(pub Box<[ValType]>);

#[derive(Debug, Clone, PartialEq)]
pub struct FuncType {
    pub params: ResultType,
    pub results: ResultType,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Limits {
    pub min: u32,
    pub max: Option<u32>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct MemType(pub Limits);

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct TableType(pub Limits, pub ElemType);

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ElemType {
    FuncRef,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct GlobalType(pub Mut, pub ValType);

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Mut {
    Const,
    Var,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ExternType {
    Func(FuncType),
    Table(TableType),
    Mem(MemType),
    Global(GlobalType),
}

pub(crate) fn funcs<'a>(extern_types: impl IntoIterator<Item = &'a ExternType>) -> Box<[FuncType]> {
    extern_types
        .into_iter()
        .filter_map(|t| match t {
            ExternType::Func(t) => Some(t.clone()),
            _ => None,
        })
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

pub(crate) fn tables<'a>(
    extern_types: impl IntoIterator<Item = &'a ExternType>,
) -> Box<[TableType]> {
    extern_types
        .into_iter()
        .filter_map(|t| match t {
            ExternType::Table(t) => Some(*t),
            _ => None,
        })
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

pub(crate) fn mems<'a>(extern_types: impl IntoIterator<Item = &'a ExternType>) -> Box<[MemType]> {
    extern_types
        .into_iter()
        .filter_map(|t| match t {
            ExternType::Mem(t) => Some(*t),
            _ => None,
        })
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

pub(crate) fn globals<'a>(
    extern_types: impl IntoIterator<Item = &'a ExternType>,
) -> Box<[GlobalType]> {
    extern_types
        .into_iter()
        .filter_map(|t| match t {
            ExternType::Global(t) => Some(*t),
            _ => None,
        })
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

impl ValType {
    pub fn bit_width(&self) -> u8 {
        use ValType::*;
        match self {
            I32 => 32,
            I64 => 64,
            F32 => 32,
            F64 => 64,
        }
    }
}

impl ResultType {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn iter(&self) -> impl Iterator<Item = &ValType> {
        self.0.iter()
    }
}

impl From<Vec<ValType>> for ResultType {
    fn from(src: Vec<ValType>) -> Self {
        Self(src.into_boxed_slice())
    }
}

//////

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Block {
    Value(ValType),
    NoResult,
}

impl FuncType {
    pub fn to_block_results(&self) -> Block {
        match &self.results.0[..] {
            &[] => Block::NoResult,
            &[value] => Block::Value(value),
            _ => panic!("more than 1 return types not supported"),
        }
    }
}
