use super::runtime::*;
use crate::structure::{instructions, modules::*, types};
use std::sync::Arc;
use thiserror::Error;
use types::ExternType;

const PAGE_SIZE: usize = 65536;

#[derive(Debug, Error)]
pub enum Error {
    #[error("module is not valid")]
    InvalidModule,
    #[error("length of the module imports does not match the provided external values")]
    ImportLenthMissMatch,
    #[error("typs of the module imports does not match the provided external values type")]
    ImportTypeMissMatch,
}

impl Store {
    pub(crate) fn instantiate(
        &mut self,
        module: &Module,
        extern_vals: &[ExternVal],
    ) -> std::result::Result<Arc<ModuleInst>, Error> {
        // TODO check module is valid => Error::InvalidModule

        if module.imports.len() != extern_vals.len() {
            return Err(Error::ImportLenthMissMatch);
        }

        for (&extern_val, extern_type) in extern_vals.iter().zip(module.extern_types()) {
            self.assert_type(extern_val, extern_type)?;
        }

        let imported_globals = globals(extern_vals);

        let module_inst = self.alloc_module(
            module,
            &imported_globals,
            tables(extern_vals),
            mems(extern_vals),
            funcs(extern_vals),
        )?;

        self.init_globals(
            &module_inst.global_addrs[imported_globals.len()..],
            &module.globals,
            &imported_globals,
        );
        self.init_tables(&module_inst, &module.elem, &imported_globals);
        self.init_mems(&module_inst.mem_addrs, &module.data, &imported_globals);

        Ok(module_inst)
    }

    pub(crate) fn alloc_global(&mut self, global: GlobalInst) -> GlobalAddr {
        self.globals.push(global);
        GlobalAddr(self.globals.len() - 1)
    }

    pub(crate) fn alloc_table(&mut self, table: TableInst) -> TableAddr {
        self.tables.push(table);
        TableAddr(self.tables.len() - 1)
    }

    pub(crate) fn alloc_mem(&mut self, mem: MemInst) -> MemAddr {
        self.mems.push(mem);
        MemAddr(self.mems.len() - 1)
    }

    pub(crate) fn alloc_func(&mut self, func: FuncInst) -> FuncAddr {
        self.funcs.push(func);
        FuncAddr(self.funcs.len() - 1)
    }

    fn init_globals(
        &mut self,
        global_addrs: &[GlobalAddr],
        globals: &[Global],
        imported_globals: &[GlobalAddr],
    ) {
        for (idx, global) in globals.iter().enumerate() {
            let value = self
                .execute_constant_expression(&imported_globals, &global.init)
                .unwrap();
            self.globals[global_addrs[idx].0].value = value;
        }
    }

    fn init_tables(
        &mut self,
        module_inst: &Arc<ModuleInst>,
        elems: &[Elem],
        imported_globals: &[GlobalAddr],
    ) {
        for elem in elems {
            assert_eq!(elem.table.0, 0, "only one table currently supported");
            let offset = u32::from(
                self.execute_constant_expression(&imported_globals, &elem.offset)
                    .expect("no value on stack"),
            );
            let table = self
                .tables_mut(module_inst.table_addrs[elem.table.0 as usize])
                .unwrap();
            for (index, &function_index) in elem.init.iter().enumerate() {
                table.elem[offset as usize + index] = Some(module_inst.func_addr(function_index))
            }
        }
    }

    fn init_mems(
        &mut self,
        mem_addrs: &[MemAddr],
        datas: &[Data],
        imported_globals: &[GlobalAddr],
    ) {
        for data in datas {
            assert_eq!(data.data.0, 0, "only one mem currently supported");
            let offset = u32::from(
                self.execute_constant_expression(&imported_globals, &data.offset)
                    .expect("no value on stack"),
            );
            let mem = self.mems_mut(mem_addrs[data.data.0 as usize]).unwrap();
            for (index, &byte) in data.init.iter().enumerate() {
                mem.data[offset as usize + index] = byte;
            }
        }
    }

    fn alloc_module(
        &mut self,
        module: &Module,
        imported_globals: &[GlobalAddr],
        imported_tables: Vec<TableAddr>,
        imported_mems: Vec<MemAddr>,
        imported_funcs: Vec<FuncAddr>,
    ) -> std::result::Result<Arc<ModuleInst>, Error> {
        fn join<T, B>(
            imported: Vec<T>,
            iter: impl Iterator<Item = B>,
            func: impl Fn(usize) -> T,
        ) -> Box<[T]> {
            let mut i = imported;
            i.extend(iter.enumerate().map(|(idx, _)| func(idx)));
            i.into_boxed_slice()
        }

        let types = module.types.clone().into_boxed_slice();

        let global_start_addr = self.globals.len();
        let global_addrs = join(imported_globals.to_vec(), module.globals.iter(), |idx| {
            GlobalAddr(global_start_addr + idx)
        });
        self.globals
            .extend(module.globals.iter().map(GlobalInst::from));

        let table_start_addr = self.tables.len();
        let table_addrs = join(imported_tables, module.tables.iter(), |idx| {
            TableAddr(table_start_addr + idx)
        });
        self.tables
            .extend(module.tables.iter().map(TableInst::from));

        let mem_start_addr = self.mems.len();
        let mem_addrs = join(imported_mems, module.mems.iter(), |idx| {
            MemAddr(mem_start_addr + idx)
        });
        self.mems.extend(module.mems.iter().map(MemInst::from));

        let func_start_addr = self.funcs.len();
        let func_addrs = join(imported_funcs, module.funcs.iter(), |idx| {
            FuncAddr(func_start_addr + idx)
        });

        let exports = module
            .exports
            .iter()
            .cloned()
            .map(|export| {
                let value = match export.desc {
                    ExportDesc::Global(index) => ExternVal::Global(global_addrs[index.0]),
                    ExportDesc::Table(index) => ExternVal::Table(table_addrs[index.0]),
                    ExportDesc::Mem(index) => ExternVal::Mem(mem_addrs[index.0]),
                    ExportDesc::Func(index) => ExternVal::Func(func_addrs[index.0]),
                };
                ExportInst {
                    name: export.name.clone(),
                    value,
                }
            })
            .collect::<Vec<_>>()
            .into_boxed_slice();

        let module_inst = Arc::new(ModuleInst {
            types,
            global_addrs,
            table_addrs,
            mem_addrs,
            func_addrs,
            exports,
        });

        self.funcs
            .extend(module.funcs.iter().map(|func| FuncInst::Module {
                func_type: module.types(func.type_idx).unwrap().clone(),
                module: module_inst.clone(),
                code: func.clone(),
            }));

        Ok(module_inst)
    }

    fn execute_constant_expression(
        &self,
        imported_globals: &[GlobalAddr],
        instructions: &ConstExpr,
    ) -> Option<Value> {
        use instructions::ConstInstr::*;
        let mut stack = Vec::new();
        for instruction in &instructions.0 {
            match instruction {
                &I32Const(value) => stack.push(value.into()),
                &I64Const(value) => stack.push(value.into()),
                &F32Const(value) => stack.push((f32::from_bits(value)).into()),
                &F64Const(value) => stack.push((f64::from_bits(value)).into()),
                &GetGlobal(value) => stack.push(
                    self.global(*imported_globals.get(value as usize).unwrap())
                        .value,
                ),
                &End => return stack.pop(),
            }
        }
        panic!("ran out of instructions before finding 'end'");
    }

    pub(crate) fn assert_type(
        &self,
        extern_val: ExternVal,
        extern_type: ExternType,
    ) -> Result<(), Error> {
        fn assert_limit(actual: types::Limits, expected: types::Limits) -> Result<(), Error> {
            if actual.min < expected.min {
                return Err(Error::ImportTypeMissMatch);
            }
            if let (Some(actual_max), Some(expected_max)) = (actual.max, expected.max) {
                if actual_max > expected_max {
                    return Err(Error::ImportTypeMissMatch);
                }
            }
            Ok(())
        }
        match extern_val {
            ExternVal::Func(addr) => {
                if extern_type != ExternType::Func(self.funcs(addr).unwrap().types().clone()) {
                    return Err(Error::ImportTypeMissMatch);
                }
            }
            ExternVal::Table(addr) => match extern_type {
                ExternType::Table(expected) => {
                    let actual = self.table(addr).types();
                    assert_limit(actual.0, expected.0)?;
                }
                _ => return Err(Error::ImportTypeMissMatch),
            },
            ExternVal::Mem(addr) => match extern_type {
                ExternType::Mem(expected) => {
                    let actual = self.mem(addr).types();
                    assert_limit(actual.0, expected.0)?;
                }
                _ => return Err(Error::ImportTypeMissMatch),
            },
            ExternVal::Global(addr) => {
                if extern_type != ExternType::Global(self.global(addr).types()) {
                    return Err(Error::ImportTypeMissMatch);
                }
            }
        }
        Ok(())
    }
}

impl From<&Table> for TableInst {
    fn from(src: &Table) -> Self {
        Self {
            elem: vec![None; src.table_type.0.min as usize].into_boxed_slice(),
            max: src.table_type.0.max,
        }
    }
}

impl From<&Mem> for MemInst {
    fn from(src: &Mem) -> Self {
        Self {
            data: vec![0; src.mem_type.0.min as usize * PAGE_SIZE],
            max: src.mem_type.0.max,
        }
    }
}

impl From<&Global> for GlobalInst {
    fn from(src: &Global) -> Self {
        Self {
            value: Value::default_from_type(src.global_type.1),
            mutable: src.global_type.0,
        }
    }
}
