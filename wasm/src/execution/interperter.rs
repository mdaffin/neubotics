use crate::{
    execution::runtime::*,
    structure::{
        instructions::{self, Instr},
        modules::{self, FuncIdx, InstructionIndex},
    },
    types,
};
use std::{collections::HashMap, sync::Arc};
use thiserror::Error;

const PAGE_SIZE: usize = 65536;

#[derive(Default)]
pub struct Interpreter {
    store: Store,
    stack: Stack,
    result: Option<Result<Vec<Value>, Trap>>,
}

#[derive(Default, Debug)]
struct Stack(Vec<StackFrame>);

#[derive(Debug)]
pub struct StackFrame {
    locals: Box<[Value]>,
    function_addr: FuncAddr,
    instruction_pointer: InstructionIndex,
    blocks: Vec<Block>,
}

#[derive(Default, Debug)]
pub struct Registry {
    modules: HashMap<String, Arc<ModuleInst>>,
}

#[derive(Debug)]
pub struct Block {
    continuation: Continuation,
    return_type: types::Block,
    values: Vec<Value>,
}

#[derive(Debug)]
pub(crate) enum Continuation {
    Start(InstructionIndex),
    End,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub(crate) struct ModuleIndex(pub usize);

enum Control {
    Jump,
    JumpTo(InstructionIndex),
    Continue,
    Return,
}
use Control::*;

type InstructionResult = std::result::Result<Control, Trap>;

impl Registry {
    pub fn add_module(&mut self, name: impl Into<String>, module: Arc<ModuleInst>) {
        self.modules.insert(name.into(), module);
    }

    pub fn get(&self, name: &str) -> Option<Arc<ModuleInst>> {
        self.modules.get(name).cloned()
    }
}

impl Block {
    pub fn get_return_value(&self) -> Option<Value> {
        match self.return_type {
            types::Block::NoResult => None,
            types::Block::Value(value) => {
                let return_value = *self.values.last().expect("no value found on stack");
                assert!(
                    match value {
                        types::ValType::I32 => return_value.is_i32(),
                        types::ValType::I64 => return_value.is_i64(),
                        types::ValType::F32 => return_value.is_f32(),
                        types::ValType::F64 => return_value.is_f64(),
                    },
                    "wrong value type on top of the stack"
                );
                Some(return_value)
            }
        }
    }
}

impl Interpreter {
    pub fn load_module(
        &mut self,
        module: &modules::Module,
        imports: &[ExternVal],
    ) -> Arc<ModuleInst> {
        assert!(!self.is_running());
        let module_inst = self.store.instantiate(module, imports).unwrap();

        if let Some(start) = &module.start {
            self.call_func_addr(module_inst.func_addr(start.func), &[])
                .unwrap();
        }

        module_inst
    }

    pub fn call(
        &mut self,
        module: Arc<ModuleInst>,
        function_name: &str,
        parameters: &[Value],
    ) -> std::result::Result<(), CallError> {
        let function_addr = match module.export(function_name) {
            Some(ExternVal::Func(addr)) => addr,
            _ => {
                return Err(CallError::FunctionNotFound {
                    module_name: "TODO".to_string(),
                    function_name: function_name.to_string(),
                })
            }
        };
        self.call_func_addr(function_addr, parameters)
    }

    fn call_func_addr(
        &mut self,
        function_addr: FuncAddr,
        parameters: &[Value],
    ) -> std::result::Result<(), CallError> {
        let actual_param_types = types::ResultType::from(
            parameters
                .iter()
                .map(|v| match v {
                    Value::I32(_) => types::ValType::I32,
                    Value::I64(_) => types::ValType::I64,
                    Value::F32(_) => types::ValType::F32,
                    Value::F64(_) => types::ValType::F64,
                })
                .collect::<Vec<_>>(),
        );

        let function = self.store.funcs(function_addr).unwrap();

        let expected_param_types = &function.types().params;
        if expected_param_types != &actual_param_types {
            return Err(CallError::InvalidParameterTypes {
                got: actual_param_types,
                expected: expected_param_types.clone(),
            });
        }

        let func_locals = function.locals();
        let mut locals = Vec::with_capacity(parameters.len() + func_locals.len());
        locals.extend_from_slice(parameters);
        locals.extend(func_locals.iter().map(|&l| Value::default_from_type(l)));

        self.stack.0.clear();
        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            function_addr,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: function.types().to_block_results(),
                values: Vec::new(),
            }],
        });
        self.result = None;

        Ok(())
    }

    pub fn advance(&mut self, max_instructions: u64) -> u64 {
        if !self.is_running() {
            return max_instructions;
        }
        for i in 0..max_instructions {
            self.next();
            if self.result.is_some() {
                return max_instructions - i;
            }
        }
        0
    }

    pub fn global(&mut self, global: GlobalAddr) -> GlobalInst {
        self.store.global(global)
    }

    pub fn alloc_global(&mut self, global: GlobalInst) -> GlobalAddr {
        self.store.alloc_global(global)
    }

    pub fn alloc_table(&mut self, table: TableInst) -> TableAddr {
        self.store.alloc_table(table)
    }

    pub fn alloc_mem(&mut self, mem: MemInst) -> MemAddr {
        self.store.alloc_mem(mem)
    }

    pub fn alloc_func(&mut self, func: FuncInst) -> FuncAddr {
        self.store.alloc_func(func)
    }

    pub fn results(&mut self) -> Option<Result<&[Value], Trap>> {
        match &self.result {
            Some(Ok(v)) => Some(Ok(v.as_slice())),
            &Some(Err(e)) => Some(Err(e)),
            None => None,
        }
    }

    pub fn is_running(&self) -> bool {
        !self.stack.0.is_empty()
    }

    pub fn reset(&mut self) {
        self.stack.0.clear();
        self.result = None;
    }

    fn i_drop(&mut self) -> InstructionResult {
        self.last_block_mut()
            .values
            .pop()
            .expect("no value on stack");
        Ok(Continue)
    }

    fn i_select(&mut self) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));
        let a = stack.pop().expect("no value on stack");
        let b = stack.pop().expect("no value on stack");
        assert_eq!(
            types::ValType::from(a),
            types::ValType::from(b),
            "values on stack are not the same type"
        );
        if condition == 0 {
            stack.push(a);
        } else {
            stack.push(b);
        }
        Ok(Continue)
    }

    fn i_get_local(&mut self, index: u32) -> InstructionResult {
        let frame = self.last_frame_mut();
        let block = frame.blocks.last_mut().expect("no block on stack");
        block.values.push(frame.locals[index as usize]);
        Ok(Continue)
    }

    fn i_set_local(&mut self, index: u32) -> InstructionResult {
        let value = self
            .last_block_mut()
            .values
            .pop()
            .expect("no value on stack");
        self.last_frame_mut().locals[index as usize] = value;
        Ok(Continue)
    }

    fn i_tee_local(&mut self, index: u32) -> InstructionResult {
        let &value = self
            .last_block_mut()
            .values
            .last()
            .expect("no value on stack");
        self.last_frame_mut().locals[index as usize] = value;
        Ok(Continue)
    }

    fn i_get_global(&mut self, index: u32) -> InstructionResult {
        let addr = self.current_module().global_addrs[index as usize];
        let value = self.store.global(addr).value;
        self.last_block_mut().values.push(value);
        Ok(Continue)
    }

    fn i_set_global(&mut self, index: u32) -> InstructionResult {
        let value = self
            .last_block_mut()
            .values
            .pop()
            .expect("no value on stack");
        let addr = self.current_module().global_addrs[index as usize];
        self.store.set_global(addr, value);
        Ok(Continue)
    }

    fn i_load(
        &mut self,
        l_type: types::ValType,
        _align: u32,
        offset: u32,
        options: Option<instructions::LoadOption>,
    ) -> InstructionResult {
        let i = i32::from(self.last_block_mut().values.pop().expect("stack is empty"));
        let memory = self.store.mem(self.current_module().mem_addrs[0]);
        let ea = (i as u32).checked_add(offset).ok_or(Trap)?;
        let n = options
            .map(|o| o.bit_width())
            .unwrap_or_else(|| l_type.bit_width());
        let address_max = ea.checked_add(n as u32 / 8).ok_or(Trap)?;
        if (address_max) as usize > memory.data.len() {
            return Err(Trap);
        }

        let value = memory
            .data
            .iter()
            .skip(ea as usize)
            .take(n as usize / 8)
            .enumerate()
            .fold(0, |acc, (i, b)| acc | ((*b as u64) << i as u64 * 8));

        let value = {
            use instructions::LoadOption::{S16, S32, S8};
            use types::ValType::{F32, F64, I32, I64};
            let value = match options {
                Some(S8) => (value as i8) as i64,
                Some(S16) => (value as i16) as i64,
                Some(S32) => (value as i32) as i64,
                _ => value as i64,
            };
            match l_type {
                I32 => Value::I32(value as i32),
                I64 => Value::I64(value as i64),
                F32 => Value::F32(f32::from_bits(value as u32)),
                F64 => Value::F64(f64::from_bits(value as u64)),
            }
        };

        self.last_block_mut().values.push(value);

        Ok(Continue)
    }

    fn i_store(
        &mut self,
        l_type: types::ValType,
        _align: u32,
        offset: u32,
        options: Option<instructions::StoreOption>,
    ) -> InstructionResult {
        let c = self.last_block_mut().values.pop().expect("stack is empty");
        assert_eq!(l_type, c.to_type());

        let i = i32::from(self.last_block_mut().values.pop().expect("stack is empty"));
        let memory = self.store.mem(self.current_module().mem_addrs[0]);
        let ea = (i as u32).checked_add(offset).ok_or(Trap)?;
        let n = options
            .map(|o| o.bit_width())
            .unwrap_or_else(|| c.bit_width());
        let address_max = ea.checked_add(n as u32 / 8).ok_or(Trap)?;
        if (address_max) as usize > memory.data.len() {
            return Err(Trap);
        }

        let c = match c {
            Value::I32(v) => v as u64,
            Value::I64(v) => v as u64,
            Value::F32(v) => v.to_bits() as u64,
            Value::F64(v) => v.to_bits(),
        };

        let memory = self
            .store
            .mems_mut(self.current_module().mem_addrs[0])
            .unwrap();
        let c = options
            .map(|o| c % 2u64.pow(o.bit_width() as u32))
            .unwrap_or(c);
        for (i, b) in memory
            .data
            .iter_mut()
            .skip(ea as usize)
            .take(n as usize / 8)
            .enumerate()
        {
            *b = ((c as u64 >> i as u64 * 8) & 0xFF) as u8;
        }

        Ok(Continue)
    }

    fn i_current_memory(&mut self, _: u8) -> InstructionResult {
        let sz = self
            .store
            .mem(self.current_module().mem_addrs[0])
            .data
            .len()
            / PAGE_SIZE;
        self.last_block_mut().values.push((sz as i32).into());
        Ok(Continue)
    }

    fn i_grow_memory(&mut self, _: u8) -> InstructionResult {
        let n = i32::from(
            self.last_block_mut()
                .values
                .pop()
                .expect("no value on stack"),
        );
        let memory = self
            .store
            .mems_mut(self.current_module().mem_addrs[0])
            .unwrap();
        let sz = memory.data.len() / PAGE_SIZE;
        let err = -1i32;
        let len = n as usize + sz;
        if len > 1 << 16 {
            self.last_block_mut().values.push(err.into());
            return Ok(Continue);
        }
        if let Some(max) = memory.max {
            if (max as usize) < len {
                self.last_block_mut().values.push(err.into());
                return Ok(Continue);
            }
        }
        memory.data.extend((0..(n as usize * PAGE_SIZE)).map(|_| 0));
        self.last_block_mut().values.push((sz as i32).into());
        Ok(Continue)
    }

    fn i_const(&mut self, value: impl Into<Value>) -> InstructionResult {
        self.last_block_mut().values.push(value.into());
        Ok(Continue)
    }

    fn i_unop(&mut self, in_type: types::ValType, op: instructions::UnOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);

        use instructions::{FUnOp, IUnOp, UnOp};
        values.push(match op {
            UnOp::IUnOp(op) => {
                let a = a.to_int().expect("stack value is not an int");
                match op {
                    IUnOp::Clz => a.clz(),
                    IUnOp::Ctz => a.ctz(),
                    IUnOp::Popcnt => a.popcnt(),
                }
            }
            UnOp::FUnOp(op) => {
                let a = a.to_float().expect("stack value is not a float");
                match op {
                    FUnOp::Abs => a.abs(),
                    FUnOp::Neg => a.neg(),
                    FUnOp::Sqrt => a.sqrt(),
                    FUnOp::Ceil => a.ceil(),
                    FUnOp::Floor => a.floor(),
                    FUnOp::Trunc => a.trunc(),
                    FUnOp::Nearest => a.nearest(),
                }
            }
        });
        Ok(Continue)
    }

    fn i_binop(&mut self, in_type: types::ValType, op: instructions::BinOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let b = values.pop().expect("no value on stack");
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);
        assert_eq!(b.to_type(), in_type);

        use instructions::{BinOp, FBinOp, IBinOp};
        values.push(match op {
            BinOp::IBinOp(op) => {
                let a = a.to_int().expect("top of stack is not an int");
                let b = b.to_int().expect("top of stack is not an int");
                match op {
                    IBinOp::Add => a.add(b),
                    IBinOp::Sub => a.sub(b),
                    IBinOp::Mul => a.mul(b),
                    IBinOp::DivS => a.div_s(b)?,
                    IBinOp::DivU => a.div_u(b)?,
                    IBinOp::RemS => a.rem_s(b)?,
                    IBinOp::RemU => a.rem_u(b)?,
                    IBinOp::And => a.and(b),
                    IBinOp::Or => a.or(b),
                    IBinOp::Xor => a.xor(b),
                    IBinOp::Shl => a.shl(b),
                    IBinOp::ShrS => a.shr_s(b),
                    IBinOp::ShrU => a.shr_u(b),
                    IBinOp::Rotl => a.rotl(b),
                    IBinOp::Rotr => a.rotr(b),
                }
            }
            BinOp::FBinOp(op) => {
                let a = a.to_float().expect("top of stack is not a float");
                let b = b.to_float().expect("top of stack is not a float");
                match op {
                    FBinOp::Add => a.add(b),
                    FBinOp::Sub => a.sub(b),
                    FBinOp::Mul => a.mul(b),
                    FBinOp::Div => a.div(b),
                    FBinOp::Min => a.min(b),
                    FBinOp::Max => a.max(b),
                    FBinOp::Copysign => a.copysign(b),
                }
            }
        });
        Ok(Continue)
    }

    fn i_itestop(
        &mut self,
        in_type: types::IntType,
        op: instructions::ITestOp,
    ) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values
            .pop()
            .expect("no value on stack")
            .to_int()
            .expect("top of the stack is not an int");
        assert_eq!(a.to_type(), in_type);

        use instructions::ITestOp;
        values.push(match op {
            ITestOp::Eqz => a.eqz(),
        });
        Ok(Continue)
    }

    fn i_relop(&mut self, in_type: types::ValType, op: instructions::RelOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let b = values.pop().expect("no value on stack");
        let a = values.pop().expect("no value on stack");
        assert_eq!(a.to_type(), in_type);
        assert_eq!(b.to_type(), in_type);

        use instructions::{FRelOp, IRelOp, RelOp};
        values.push(match op {
            RelOp::IRelOp(op) => {
                let a = a.to_int().expect("top of stack is not an int");
                let b = b.to_int().expect("top of stack is not an int");
                match op {
                    IRelOp::Eq => a.eq(b),
                    IRelOp::Ne => a.ne(b),
                    IRelOp::LtS => a.lt_s(b),
                    IRelOp::LtU => a.lt_u(b),
                    IRelOp::GtS => a.gt_s(b),
                    IRelOp::GtU => a.gt_u(b),
                    IRelOp::LeS => a.le_s(b),
                    IRelOp::LeU => a.le_u(b),
                    IRelOp::GeS => a.ge_s(b),
                    IRelOp::GeU => a.ge_u(b),
                }
            }
            RelOp::FRelOp(op) => {
                let a = a.to_float().expect("top of stack is not a float");
                let b = b.to_float().expect("top of stack is not a float");
                match op {
                    FRelOp::Eq => a.eq(b),
                    FRelOp::Ne => a.ne(b),
                    FRelOp::Lt => a.lt(b),
                    FRelOp::Gt => a.gt(b),
                    FRelOp::Le => a.le(b),
                    FRelOp::Ge => a.ge(b),
                }
            }
        });
        Ok(Continue)
    }

    fn i_cvtop(&mut self, op: instructions::CvtOp) -> InstructionResult {
        let values = &mut self.last_block_mut().values;
        let a = values.pop().expect("no value on stack");

        use instructions::CvtOp;
        use types::{FloatType as FT, IntType as IT};
        values.push(match op {
            CvtOp::I32WrapI64 => Value::I32(a.to_i64().unwrap() as i32),
            CvtOp::I64ExtendSI32 => Value::I64(i64::from(a.to_i32().unwrap())),
            CvtOp::I64ExtendUI32 => Value::I64(i64::from(a.to_i32().unwrap() as u32)),
            CvtOp::Trunc { from, to, signed } => match (from, to, signed) {
                (FT::F32, IT::I32, true) => {
                    let a = a.to_f32().unwrap();
                    if a.is_nan() || a >= i32::MAX as f32 || a < i32::MIN as f32 {
                        return Err(Trap);
                    }
                    Value::I32(a as i32)
                }
                (FT::F32, IT::I32, false) => {
                    let a = a.to_f32().unwrap();
                    if a.is_nan() || a >= u32::MAX as f32 || a <= -1.0 {
                        return Err(Trap);
                    }
                    Value::I32((a as u32) as i32)
                }
                (FT::F32, IT::I64, true) => {
                    let a = a.to_f32().unwrap();
                    if a.is_nan() || a >= i64::MAX as f32 || a < i64::MIN as f32 {
                        return Err(Trap);
                    }
                    Value::I64(a as i64)
                }
                (FT::F32, IT::I64, false) => {
                    let a = a.to_f32().unwrap();
                    if a.is_nan() || a >= u64::MAX as f32 || a <= -1.0 {
                        return Err(Trap);
                    }
                    Value::I64((a as u64) as i64)
                }
                (FT::F64, IT::I32, true) => {
                    let a = a.to_f64().unwrap();
                    if a.is_nan() || a > i32::MAX as f64 || a < i32::MIN as f64 {
                        return Err(Trap);
                    }
                    Value::I32(a as i32)
                }
                (FT::F64, IT::I32, false) => {
                    let a = a.to_f64().unwrap();
                    if a.is_nan() || a > u32::MAX as f64 || a <= -1.0 {
                        return Err(Trap);
                    }
                    Value::I32((a as u32) as i32)
                }
                (FT::F64, IT::I64, true) => {
                    let a = a.to_f64().unwrap();
                    if a.is_nan() || a >= i64::MAX as f64 || a < i64::MIN as f64 {
                        return Err(Trap);
                    }
                    Value::I64(a as i64)
                }
                (FT::F64, IT::I64, false) => {
                    let a = a.to_f64().unwrap();
                    if a.is_nan() || a >= u64::MAX as f64 || a <= -1.0 {
                        return Err(Trap);
                    }
                    Value::I64((a as u64) as i64)
                }
            },
            CvtOp::Convert { from, to, signed } => match (from, to, signed) {
                (IT::I32, FT::F32, true) => Value::F32(a.to_i32().unwrap() as f32),
                (IT::I32, FT::F32, false) => Value::F32((a.to_i32().unwrap() as u32) as f32),
                (IT::I32, FT::F64, true) => Value::F64(a.to_i32().unwrap() as f64),
                (IT::I32, FT::F64, false) => Value::F64((a.to_i32().unwrap() as u32) as f64),
                (IT::I64, FT::F32, true) => Value::F32(a.to_i64().unwrap() as f32),
                (IT::I64, FT::F32, false) => Value::F32((a.to_i64().unwrap() as u64) as f32),
                (IT::I64, FT::F64, true) => Value::F64(a.to_i64().unwrap() as f64),
                (IT::I64, FT::F64, false) => Value::F64((a.to_i64().unwrap() as u64) as f64),
            },
            CvtOp::F32DemoteF64 => Value::F32(a.to_f64().unwrap() as f32),
            CvtOp::F64PromoteF32 => Value::F64(a.to_f32().unwrap() as f64),
            CvtOp::I32ReinterpretF32 => {
                Value::I32(i32::from_ne_bytes(a.to_f32().unwrap().to_ne_bytes()))
            }
            CvtOp::I64ReinterpretF64 => {
                Value::I64(i64::from_ne_bytes(a.to_f64().unwrap().to_ne_bytes()))
            }
            CvtOp::F32ReinterpretI32 => {
                Value::F32(f32::from_ne_bytes(a.to_i32().unwrap().to_ne_bytes()))
            }
            CvtOp::F64ReinterpretI64 => {
                Value::F64(f64::from_ne_bytes(a.to_i64().unwrap().to_ne_bytes()))
            }
        });
        Ok(Continue)
    }

    fn i_block(&mut self, block: types::Block) -> InstructionResult {
        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::End,
            return_type: block,
            values: Vec::new(),
        });
        Ok(Continue)
    }

    fn i_loop(&mut self, block: types::Block) -> InstructionResult {
        let ip = self.last_frame().instruction_pointer;
        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::Start(ip),
            return_type: block,
            values: Vec::new(),
        });
        Ok(Continue)
    }

    fn i_if(&mut self, block: types::Block) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));

        self.last_frame_mut().blocks.push(Block {
            continuation: Continuation::End,
            return_type: block,
            values: Vec::new(),
        });

        if condition != 0 {
            Ok(Continue)
        } else {
            let body = &self.current_function().body;
            let mut acc = 0;
            let (pos, instruction) = body
                .0
                .iter()
                .enumerate()
                .skip(self.last_frame().instruction_pointer.0 + 1)
                .find(|(_, instruction)| {
                    acc += match instruction {
                        Instr::End => -1,
                        Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => 1,
                        _ => 0,
                    };
                    (acc == 0 && *instruction == &Instr::Else)
                        || (acc < 0 && *instruction == &Instr::End)
                })
                .expect("could not find labeled end instruction");

            match instruction {
                Instr::Else => Ok(JumpTo(InstructionIndex(pos + 1))),
                Instr::End => Ok(JumpTo(InstructionIndex(pos))),
                _ => panic!("expected else or end instruction"),
            }
        }
    }

    fn i_else(&mut self) -> InstructionResult {
        let body = &self.current_function().body;
        let mut acc = 0;
        let (index, _) = body
            .0
            .iter()
            .enumerate()
            .skip(self.last_frame().instruction_pointer.0 + 1)
            .find(|(_, instruction)| {
                acc += match instruction {
                    Instr::End => -1,
                    Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => 1,
                    _ => 0,
                };
                acc < 0 && *instruction == &Instr::End
            })
            .expect("could not find labeled end instruction");

        Ok(JumpTo(InstructionIndex(index)))
    }

    fn i_return(&mut self) -> InstructionResult {
        let mut frame = self.stack.0.pop().expect("no frame on stack");

        let return_value = frame
            .blocks
            .last_mut()
            .expect("no frame on stack")
            .values
            .pop();

        if let Some(return_value) = return_value {
            frame
                .blocks
                .first_mut()
                .expect("no frame on stack")
                .values
                .push(return_value);
        }

        let return_value = frame
            .blocks
            .first()
            .expect("no frame on stack")
            .get_return_value();

        match (self.stack.0.is_empty(), return_value) {
            (true, Some(return_value)) => self.result = Some(Ok(vec![return_value])),
            (true, None) => self.result = Some(Ok(Vec::new())),
            (false, Some(return_value)) => self.last_block_mut().values.push(return_value),
            _ => (),
        }

        Ok(Return)
    }

    fn i_call(&mut self, function_index: u32) -> InstructionResult {
        let module = self.current_module();
        let function_index = FuncIdx(function_index as usize);

        let stack = &mut self.stack;

        let function_addr = module.func_addr(function_index);
        let function = self.store.funcs(function_addr).unwrap();
        let function_type = function.types();

        let mut locals = Vec::with_capacity(function_type.params.len() + function.locals().len());
        locals.extend({
            let values = &mut stack
                .0
                .last_mut()
                .expect("no value on stack")
                .blocks
                .last_mut()
                .expect("no block on stack frame")
                .values;
            values.split_off(values.len() - function_type.params.len())
        });

        function_type
            .params
            .iter()
            .zip(locals.iter())
            .for_each(|(&param_type, &stack_value)| {
                assert_eq!(types::ValType::from(stack_value), param_type);
            });
        locals.extend(
            function
                .locals()
                .iter()
                .map(|&l| Value::default_from_type(l)),
        );

        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            function_addr,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: function_type.to_block_results(),
                values: Vec::new(),
            }],
        });
        Ok(Jump)
    }

    fn i_call_indirect(&mut self, type_idx: u32, _: u8) -> InstructionResult {
        let index = i32::from(
            self.last_block_mut()
                .values
                .pop()
                .expect("no value on stack"),
        );

        let module = self.current_module();
        let tab = self.store.table(module.table_addrs[0]);
        let expected_function_type = &module.types[type_idx as usize];

        if index as usize >= tab.elem.len() {
            return Err(Trap);
        }

        let function_addr = tab.elem[index as usize].ok_or(Trap)?;
        let function = self.store.funcs(function_addr).unwrap();
        let actual_function_type = function.types();
        if actual_function_type != expected_function_type {
            return Err(Trap);
        }

        let mut locals =
            Vec::with_capacity(actual_function_type.params.len() + function.locals().len());
        let parameters = {
            let values = &mut self
                .stack
                .0
                .last_mut()
                .expect("no value on stack")
                .blocks
                .last_mut()
                .expect("no block on stack frame")
                .values;
            values.split_off(values.len() - actual_function_type.params.len())
        };
        locals.extend(
            actual_function_type
                .params
                .iter()
                .zip(parameters.iter())
                .map(|(&p_type, &value)| {
                    assert_eq!(types::ValType::from(value), p_type);
                    value
                }),
        );
        locals.extend(
            function
                .locals()
                .iter()
                .map(|&l| Value::default_from_type(l)),
        );

        self.stack.0.push(StackFrame {
            locals: locals.into_boxed_slice(),
            function_addr,
            instruction_pointer: Default::default(),
            blocks: vec![Block {
                continuation: Continuation::End,
                return_type: actual_function_type.to_block_results(),
                values: Vec::new(),
            }],
        });
        Ok(Jump)
    }

    fn i_end(&mut self) -> InstructionResult {
        let block = self
            .last_frame_mut()
            .blocks
            .pop()
            .expect("no block on stack");
        let return_value = block.get_return_value();

        let control = if self.last_frame().blocks.is_empty() {
            self.stack.0.pop().expect("no frame on stack");
            Return
        } else {
            Continue
        };

        match (self.stack.0.is_empty(), return_value) {
            (true, Some(return_value)) => self.result = Some(Ok(vec![return_value])),
            (true, None) => self.result = Some(Ok(Vec::new())),
            (false, Some(return_value)) => self.last_block_mut().values.push(return_value),
            _ => (),
        }

        Ok(control)
    }

    fn i_br(&mut self, l: u32) -> InstructionResult {
        let return_value = self.last_block_mut().values.pop();

        for _ in 0..l {
            self.last_frame_mut()
                .blocks
                .pop()
                .expect("not enough blocks on the stack");
        }

        if let Some(value) = return_value {
            self.last_block_mut().values.push(value);
        }

        let body = &self.current_function().body;
        let ip = self.last_frame().instruction_pointer.0;
        Ok(JumpTo(match self.last_block().continuation {
            Continuation::Start(ip) => InstructionIndex(ip.0 + 1),
            Continuation::End => {
                let mut acc = l as i32;
                let (pos, _) = body.0[ip..]
                    .iter()
                    .enumerate()
                    .filter_map(|(i, instruction)| match instruction {
                        Instr::End => Some((i, -1)),
                        Instr::If(_) | Instr::Block(_) | Instr::Loop(_) => Some((i, 1)),
                        _ => None,
                    })
                    .find(|(_, v)| {
                        acc += v;
                        acc < 0
                    })
                    .expect("could not find labeled end instruction");
                InstructionIndex(ip + pos)
            }
        }))
    }

    fn i_br_if(&mut self, l: u32) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let condition = i32::from(stack.pop().expect("no value on stack"));

        if condition != 0 {
            self.i_br(l)
        } else {
            Ok(Continue)
        }
    }

    fn i_br_table(&mut self, table: &[u32], default: u32) -> InstructionResult {
        let stack = &mut self.last_block_mut().values;
        let index = i32::from(stack.pop().expect("no value on stack"));
        if let Some(&l) = table.get(index as usize) {
            self.i_br(l)
        } else {
            self.i_br(default)
        }
    }

    fn next(&mut self) {
        //self.dbg_memory(self.current_module());
        //self.dbg_tables();
        //self.dbg_stack();

        enum Code {
            Module(Instr),
            Host(types::FuncType, HostFunc),
        };

        let code = match self.store.funcs(self.last_frame().function_addr).unwrap() {
            FuncInst::Module { code, .. } => {
                Code::Module(code.instruction(self.last_frame().instruction_pointer))
            }
            FuncInst::Host {
                func_type,
                host_code,
            } => Code::Host(func_type.clone(), host_code.clone()),
        };

        match code {
            Code::Module(instruction) => self.n_execute_instruction(instruction),
            Code::Host(func_type, host_func) => self.n_execute_host_func(func_type, host_func),
        }
    }

    fn n_execute_host_func(&mut self, func_type: types::FuncType, host_code: HostFunc) {
        let params = &self.last_frame().locals;
        let mut results = func_type
            .results
            .iter()
            .map(|&result| Value::default_from_type(result))
            .collect::<Vec<Value>>();

        host_code.lock().unwrap()(params, &mut results).unwrap();

        self.stack.0.pop();

        if self.stack.0.is_empty() {
            self.result = Some(Ok(results));
        } else {
            for result in results {
                self.last_block_mut().values.push(result);
            }
            self.last_frame_mut().instruction_pointer.0 += 1;
        }
    }

    fn n_execute_instruction(&mut self, instruction: Instr) {
        //println!("## Executing instruction {:?}", &instruction);

        let result = match instruction {
            Instr::Unreachable => Err(Trap),
            Instr::Nop => Ok(Continue),
            Instr::Const(v) => self.i_const(v),
            Instr::Block(block) => self.i_block(block),
            Instr::Loop(block) => self.i_loop(block),
            Instr::If(block) => self.i_if(block),
            Instr::Else => self.i_else(),
            Instr::End => self.i_end(),
            Instr::Br(l) => self.i_br(l),
            Instr::BrIf(l) => self.i_br_if(l),
            Instr::BrTable(table, default) => self.i_br_table(&table, default),
            Instr::Return => self.i_return(),
            Instr::Call(function_index) => self.i_call(function_index),
            Instr::CallIndirect(x, y) => self.i_call_indirect(x, y),
            Instr::Drop => self.i_drop(),
            Instr::Select => self.i_select(),
            Instr::GetLocal(local_index) => self.i_get_local(local_index),
            Instr::SetLocal(local_index) => self.i_set_local(local_index),
            Instr::TeeLocal(local_index) => self.i_tee_local(local_index),
            Instr::GetGlobal(global_index) => self.i_get_global(global_index),
            Instr::SetGlobal(global_index) => self.i_set_global(global_index),
            Instr::Load {
                l_type,
                align,
                offset,
                options,
            } => self.i_load(l_type, align, offset, options),
            Instr::Store {
                l_type,
                align,
                offset,
                options,
            } => self.i_store(l_type, align, offset, options),
            Instr::CurrentMemory(a) => self.i_current_memory(a),
            Instr::GrowMemory(pages) => self.i_grow_memory(pages),
            Instr::UnOp(t, op) => self.i_unop(t, op),
            Instr::BinOp(t, op) => self.i_binop(t, op),
            Instr::ITestOp(t, op) => self.i_itestop(t, op),
            Instr::RelOp(t, op) => self.i_relop(t, op),
            Instr::CvtOp(op) => self.i_cvtop(op),
        };

        match result {
            Ok(Jump) => (),
            Ok(JumpTo(index)) => *self.instruction_pointer_mut() = index,
            Ok(Return) => {
                if let Some(frame) = self.stack.0.last_mut() {
                    frame.instruction_pointer.increment();
                }
            }
            Ok(Continue) => self.instruction_pointer_mut().increment(),
            Err(trap) => {
                self.stack.0.clear();
                self.result = Some(Err(trap));
            }
        }
    }

    fn last_frame(&self) -> &StackFrame {
        self.stack.0.last().expect("no frames on the stack")
    }

    fn last_frame_mut(&mut self) -> &mut StackFrame {
        self.stack.0.last_mut().expect("no frames on the stack")
    }

    fn last_block(&self) -> &Block {
        self.last_frame()
            .blocks
            .last()
            .expect("no blocks on the stack")
    }

    fn last_block_mut(&mut self) -> &mut Block {
        self.last_frame_mut()
            .blocks
            .last_mut()
            .expect("no blocks on the stack")
    }

    fn instruction_pointer_mut(&mut self) -> &mut InstructionIndex {
        &mut self.last_frame_mut().instruction_pointer
    }

    fn current_module(&self) -> Arc<ModuleInst> {
        match self.store.funcs(self.last_frame().function_addr).unwrap() {
            FuncInst::Module { module, .. } => module.clone(),
            FuncInst::Host { .. } => unreachable!(),
        }
    }

    fn current_function(&self) -> &modules::Func {
        match self.store.funcs(self.last_frame().function_addr).unwrap() {
            FuncInst::Module { code, .. } => code,
            FuncInst::Host { .. } => unreachable!(),
        }
    }
}

#[derive(Error, Debug)]
pub enum CallError {
    #[error("module not found: {module_name}")]
    ModuleNotFound { module_name: String },
    #[error("function not found in module '{module_name}': {function_name}")]
    FunctionNotFound {
        module_name: String,
        function_name: String,
    },
    #[error(
        "supplied parameters do not match the function type got {got:?} expected {expected:?}"
    )]
    InvalidParameterTypes {
        got: types::ResultType,
        expected: types::ResultType,
    },
}

impl Interpreter {
    pub fn dbg_memory(&self, module: Arc<ModuleInst>) {
        print!("Memory {{");
        for &memory in module.mem_addrs.iter() {
            let memory = self.store.mem(memory);
            print!(
                "({}) {:?}",
                memory.data.len() / PAGE_SIZE,
                &memory.data[..memory.data.len().min(20)]
            );
        }
        println!("}}");
    }

    pub fn dbg_tables(&self) {
        let module = self.current_module();
        print!("Tables {{");
        for &table_addr in module.table_addrs.iter() {
            let table = self.store.table(table_addr);
            print!(
                "[{}]",
                table
                    .elem
                    .iter()
                    .enumerate()
                    .map(|(i, t)| match t {
                        Some(v) => format!("{}:{}", i, v.0),
                        None => format!("{}:-", i),
                    })
                    .collect::<Vec<_>>()
                    .join(", ")
            );
        }
        println!("}}");
    }

    pub fn dbg_globals(&self) {
        print!("Global {{");
        println!(
            "{}",
            self.store
                .globals
                .iter()
                .map(|global| format!("{:?}", global))
                .collect::<Vec<_>>()
                .join(", ")
        );
        println!("}}");
    }

    pub fn dbg_stack(&self) {
        println!("Stack {{");
        for frame in self.stack.0.iter() {
            println!(
                "  Frame func {} ip: {} locals {:?} {{",
                frame.function_addr.0, frame.instruction_pointer.0, frame.locals,
            );
            for block in frame.blocks.iter() {
                println!(
                    "    Block c{:?} r{:?} {{",
                    block.continuation, block.return_type
                );
                for value in block.values.iter() {
                    println!("      {:?}", value)
                }
                println!("    }}");
            }
            println!("  }}");
        }
        println!("}}");
    }
}
