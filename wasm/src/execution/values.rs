use super::runtime::{Float, Int, Trap, Value};
use crate::types::{IntType, ValType};

impl From<Value> for ValType {
    fn from(src: Value) -> ValType {
        match src {
            Value::I32(_) => ValType::I32,
            Value::I64(_) => ValType::I64,
            Value::F32(_) => ValType::F32,
            Value::F64(_) => ValType::F64,
        }
    }
}

impl From<i32> for Value {
    fn from(src: i32) -> Self {
        Value::I32(src)
    }
}

impl From<i64> for Value {
    fn from(src: i64) -> Self {
        Value::I64(src)
    }
}

impl From<u32> for Value {
    fn from(num: u32) -> Self {
        Value::I32(num as i32)
    }
}

impl From<u64> for Value {
    fn from(num: u64) -> Self {
        Value::I64(num as i64)
    }
}

impl From<f32> for Value {
    fn from(src: f32) -> Self {
        Value::F32(src)
    }
}

impl From<f64> for Value {
    fn from(src: f64) -> Self {
        Value::F64(src)
    }
}

impl From<Value> for i32 {
    fn from(vl: Value) -> i32 {
        match vl {
            Value::I32(i) => i,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for i64 {
    fn from(vl: Value) -> i64 {
        match vl {
            Value::I64(i) => i,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for u32 {
    fn from(vl: Value) -> u32 {
        match vl {
            Value::I32(i) => i as u32,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for u64 {
    fn from(vl: Value) -> u64 {
        match vl {
            Value::I64(i) => i as u64,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for f32 {
    fn from(vl: Value) -> f32 {
        match vl {
            Value::F32(i) => i,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for f64 {
    fn from(vl: Value) -> f64 {
        match vl {
            Value::F64(i) => i,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl From<Value> for bool {
    fn from(vl: Value) -> bool {
        match vl {
            Value::I32(i) => i != 0,
            _ => panic!("Unwrap value failed"),
        }
    }
}

impl Int {
    pub fn to_type(&self) -> IntType {
        match self {
            Int::I32(_) => IntType::I32,
            Int::I64(_) => IntType::I64,
        }
    }

    pub fn clz(self) -> Value {
        match self {
            Int::I32(v) => Value::I32(v.leading_zeros() as i32),
            Int::I64(v) => Value::I64(v.leading_zeros() as i64),
        }
    }

    pub fn ctz(self) -> Value {
        match self {
            Int::I32(v) => Value::I32(v.trailing_zeros() as i32),
            Int::I64(v) => Value::I64(v.trailing_zeros() as i64),
        }
    }

    pub fn popcnt(self) -> Value {
        match self {
            Int::I32(v) => Value::I32(v.count_ones() as i32),
            Int::I64(v) => Value::I64(v.count_ones() as i64),
        }
    }

    pub fn add(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a.wrapping_add(b)),
            (Int::I64(a), Int::I64(b)) => Value::I64(a.wrapping_add(b)),
            _ => panic!("miss matched types"),
        }
    }

    pub fn sub(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a.wrapping_sub(b)),
            (Int::I64(a), Int::I64(b)) => Value::I64(a.wrapping_sub(b)),
            _ => panic!("miss matched types"),
        }
    }

    pub fn mul(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a.wrapping_mul(b)),
            (Int::I64(a), Int::I64(b)) => Value::I64(a.wrapping_mul(b)),
            _ => panic!("miss matched types"),
        }
    }

    fn is_zero(self) -> bool {
        match self {
            Int::I32(a) => a == 0,
            Int::I64(a) => a == 0,
        }
    }

    pub fn div_s(self, other: Self) -> Result<Value, Trap> {
        if other.is_zero() {
            Err(Trap)
        } else {
            Ok(match (self, other) {
                (Int::I32(a), Int::I32(b)) => Value::I32(a.checked_div(b).ok_or(Trap)?),
                (Int::I64(a), Int::I64(b)) => Value::I64(a.checked_div(b).ok_or(Trap)?),
                _ => panic!("miss matched types"),
            })
        }
    }

    pub fn div_u(self, other: Self) -> Result<Value, Trap> {
        if other.is_zero() {
            Err(Trap)
        } else {
            Ok(match (self, other) {
                (Int::I32(a), Int::I32(b)) => Value::I32((a as u32).wrapping_div(b as u32) as i32),
                (Int::I64(a), Int::I64(b)) => Value::I64((a as u64).wrapping_div(b as u64) as i64),
                _ => panic!("miss matched types"),
            })
        }
    }

    pub fn rem_s(self, other: Self) -> Result<Value, Trap> {
        if other.is_zero() {
            Err(Trap)
        } else {
            Ok(match (self, other) {
                (Int::I32(a), Int::I32(b)) => Value::I32(a.wrapping_rem(b)),
                (Int::I64(a), Int::I64(b)) => Value::I64(a.wrapping_rem(b)),
                _ => panic!("miss matched types"),
            })
        }
    }

    pub fn rem_u(self, other: Self) -> Result<Value, Trap> {
        if other.is_zero() {
            Err(Trap)
        } else {
            Ok(match (self, other) {
                (Int::I32(a), Int::I32(b)) => Value::I32((a as u32).wrapping_rem(b as u32) as i32),
                (Int::I64(a), Int::I64(b)) => Value::I64((a as u64).wrapping_rem(b as u64) as i64),
                _ => panic!("miss matched types"),
            })
        }
    }

    pub fn and(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a & b),
            (Int::I64(a), Int::I64(b)) => Value::I64(a & b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn or(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a | b),
            (Int::I64(a), Int::I64(b)) => Value::I64(a | b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn xor(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a ^ b),
            (Int::I64(a), Int::I64(b)) => Value::I64(a ^ b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn shl(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => {
                let (v, _) = a.overflowing_shl(b as u32);
                Value::I32(v)
            }
            (Int::I64(a), Int::I64(b)) => {
                let (v, _) = a.overflowing_shl(b as u32);
                Value::I64(v)
            }
            _ => panic!("miss matched types"),
        }
    }

    pub fn shr_s(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => {
                let (v, _) = a.overflowing_shr(b as u32);
                Value::I32(v)
            }
            (Int::I64(a), Int::I64(b)) => {
                let (v, _) = a.overflowing_shr(b as u32);
                Value::I64(v)
            }
            _ => panic!("miss matched types"),
        }
    }

    pub fn shr_u(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => {
                let (v, _) = (a as u32).overflowing_shr(b as u32);
                Value::I32(v as i32)
            }
            (Int::I64(a), Int::I64(b)) => {
                let (v, _) = (a as u64).overflowing_shr(b as u32);
                Value::I64(v as i64)
            }
            _ => panic!("miss matched types"),
        }
    }

    pub fn rotl(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a.rotate_left(b as u32)),
            (Int::I64(a), Int::I64(b)) => Value::I64(a.rotate_left(b as u32)),
            _ => panic!("miss matched types"),
        }
    }

    pub fn rotr(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(a.rotate_right(b as u32)),
            (Int::I64(a), Int::I64(b)) => Value::I64(a.rotate_right(b as u32)),
            _ => panic!("miss matched types"),
        }
    }

    pub fn eqz(self) -> Value {
        match self {
            Int::I32(a) => Value::I32((a == 0) as i32),
            Int::I64(a) => Value::I32((a == 0) as i32),
        }
    }

    pub fn eq(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a == b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a == b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn ne(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a != b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a != b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn lt_s(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a < b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a < b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn lt_u(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(((a as u32) < (b as u32)) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32(((a as u64) < (b as u64)) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn gt_s(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a > b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a > b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn gt_u(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(((a as u32) > (b as u32)) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32(((a as u64) > (b as u64)) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn le_s(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a <= b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a <= b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn le_u(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(((a as u32) <= (b as u32)) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32(((a as u64) <= (b as u64)) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn ge_s(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32((a >= b) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32((a >= b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn ge_u(self, other: Self) -> Value {
        match (self, other) {
            (Int::I32(a), Int::I32(b)) => Value::I32(((a as u32) >= (b as u32)) as i32),
            (Int::I64(a), Int::I64(b)) => Value::I32(((a as u64) >= (b as u64)) as i32),
            _ => panic!("miss matched types"),
        }
    }
}

impl Float {
    pub fn abs(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(a.abs()),
            Float::F64(a) => Value::F64(a.abs()),
        }
    }

    pub fn neg(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(-a),
            Float::F64(a) => Value::F64(-a),
        }
    }

    pub fn sqrt(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(a.sqrt()),
            Float::F64(a) => Value::F64(a.sqrt()),
        }
    }

    pub fn ceil(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(a.ceil()),
            Float::F64(a) => Value::F64(a.ceil()),
        }
    }

    pub fn floor(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(a.floor()),
            Float::F64(a) => Value::F64(a.floor()),
        }
    }

    pub fn trunc(self) -> Value {
        match self {
            Float::F32(a) => Value::F32(a.trunc()),
            Float::F64(a) => Value::F64(a.trunc()),
        }
    }

    pub fn nearest(self) -> Value {
        fn round<T: num::Float>(f: T, two: T) -> T {
            if f.is_zero() || f.is_nan() {
                return f;
            }

            let u = f.ceil();
            let d = f.floor();
            let um = (f - u).abs();
            let dm = (f - d).abs();
            let half_u = u / two;
            if um < dm || um == dm && half_u.floor() == half_u {
                u
            } else {
                d
            }
        }
        match self {
            Float::F32(a) => {
                println!("{:#x}", a.to_bits());
                Value::F32(round(a, 2.))
            }
            Float::F64(a) => Value::F64(round(a, 2.)),
        }
    }

    pub fn add(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(a + b),
            (Float::F64(a), Float::F64(b)) => Value::F64(a + b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn sub(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(a - b),
            (Float::F64(a), Float::F64(b)) => Value::F64(a - b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn mul(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(a * b),
            (Float::F64(a), Float::F64(b)) => Value::F64(a * b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn div(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(a / b),
            (Float::F64(a), Float::F64(b)) => Value::F64(a / b),
            _ => panic!("miss matched types"),
        }
    }

    pub fn min(self, other: Self) -> Value {
        if self.is_nan() {
            self.into()
        } else if other.is_nan() {
            other.into()
        } else if self.is_zero()
            && other.is_zero()
            && (self.is_sign_negative() || other.is_sign_negative())
        {
            match self {
                Float::F32(_) => Value::F32(-0.0),
                Float::F64(_) => Value::F64(-0.0),
            }
        } else {
            match (self, other) {
                (Float::F32(a), Float::F32(b)) => Value::F32(a.min(b)),
                (Float::F64(a), Float::F64(b)) => Value::F64(a.min(b)),
                _ => panic!("miss matched types"),
            }
        }
    }

    pub fn max(self, other: Self) -> Value {
        if self.is_nan() {
            self.into()
        } else if other.is_nan() {
            other.into()
        } else if self.is_zero()
            && other.is_zero()
            && (self.is_sign_positive() || other.is_sign_positive())
        {
            match self {
                Float::F32(_) => Value::F32(0.0),
                Float::F64(_) => Value::F64(0.0),
            }
        } else {
            match (self, other) {
                (Float::F32(a), Float::F32(b)) => Value::F32(a.max(b)),
                (Float::F64(a), Float::F64(b)) => Value::F64(a.max(b)),
                _ => panic!("miss matched types"),
            }
        }
    }

    fn is_nan(self) -> bool {
        match self {
            Float::F32(a) => a.is_nan(),
            Float::F64(a) => a.is_nan(),
        }
    }

    fn is_zero(self) -> bool {
        match self {
            Float::F32(a) => a == 0.0,
            Float::F64(a) => a == 0.0,
        }
    }

    fn is_sign_positive(self) -> bool {
        match self {
            Float::F32(a) => a.is_sign_positive(),
            Float::F64(a) => a.is_sign_positive(),
        }
    }

    fn is_sign_negative(self) -> bool {
        match self {
            Float::F32(a) => a.is_sign_negative(),
            Float::F64(a) => a.is_sign_negative(),
        }
    }

    pub fn copysign(self, other: Self) -> Value {
        fn copysign<T: num::Float>(f1: T, f2: T) -> T {
            if (f1.is_sign_positive() && f2.is_sign_positive())
                || (f1.is_sign_negative() && f2.is_sign_negative())
            {
                f1
            } else {
                f1.neg()
            }
        }

        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(copysign(a, b)),
            (Float::F64(a), Float::F64(b)) => Value::F64(copysign(a, b)),
            _ => panic!("miss matched types"),
        }
    }

    /*
    pub fn sub(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::F32(a.wrapping_sub(b)),
            (Float::F64(a), Float::F64(b)) => Value::F64(a.wrapping_sub(b)),
            _ => panic!("miss matched types"),
        }
    }
    */

    pub fn eq(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a == b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a == b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn ne(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a != b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a != b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn lt(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a < b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a < b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn gt(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a > b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a > b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn le(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a <= b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a <= b) as i32),
            _ => panic!("miss matched types"),
        }
    }

    pub fn ge(self, other: Self) -> Value {
        match (self, other) {
            (Float::F32(a), Float::F32(b)) => Value::I32((a >= b) as i32),
            (Float::F64(a), Float::F64(b)) => Value::I32((a >= b) as i32),
            _ => panic!("miss matched types"),
        }
    }
}

impl Value {
    pub fn default_from_type(src: ValType) -> Self {
        match src {
            ValType::I32 => Value::I32(0),
            ValType::I64 => Value::I64(0),
            ValType::F32 => Value::F32(0.),
            ValType::F64 => Value::F64(0.),
        }
    }

    pub fn to_type(&self) -> ValType {
        match self {
            Value::I32(_) => ValType::I32,
            Value::I64(_) => ValType::I64,
            Value::F32(_) => ValType::F32,
            Value::F64(_) => ValType::F64,
        }
    }

    pub fn to_int(&self) -> Option<Int> {
        match *self {
            Value::I32(v) => Some(Int::I32(v)),
            Value::I64(v) => Some(Int::I64(v)),
            _ => None,
        }
    }

    pub fn to_float(&self) -> Option<Float> {
        match *self {
            Value::F32(v) => Some(Float::F32(v)),
            Value::F64(v) => Some(Float::F64(v)),
            _ => None,
        }
    }

    pub fn bit_width(&self) -> u8 {
        match self {
            Value::I32(_) => 32,
            Value::I64(_) => 64,
            Value::F32(_) => 32,
            Value::F64(_) => 64,
        }
    }

    pub fn is_i32(self) -> bool {
        match self {
            Value::I32(_) => true,
            _ => false,
        }
    }

    pub fn is_i64(self) -> bool {
        match self {
            Value::I64(_) => true,
            _ => false,
        }
    }

    pub fn is_f32(self) -> bool {
        match self {
            Value::F32(_) => true,
            _ => false,
        }
    }

    pub fn is_f64(self) -> bool {
        match self {
            Value::F64(_) => true,
            _ => false,
        }
    }

    pub fn to_i32(self) -> Option<i32> {
        match self {
            Value::I32(v) => Some(v),
            _ => None,
        }
    }

    pub fn to_i64(self) -> Option<i64> {
        match self {
            Value::I64(v) => Some(v),
            _ => None,
        }
    }

    pub fn to_f32(self) -> Option<f32> {
        match self {
            Value::F32(v) => Some(v),
            _ => None,
        }
    }

    pub fn to_f64(self) -> Option<f64> {
        match self {
            Value::F64(v) => Some(v),
            _ => None,
        }
    }
}

impl From<Float> for Value {
    fn from(src: Float) -> Value {
        match src {
            Float::F32(val) => Value::F32(val),
            Float::F64(val) => Value::F64(val),
        }
    }
}
