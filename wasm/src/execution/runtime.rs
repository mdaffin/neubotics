use crate::structure::modules::*;
use crate::structure::types;
use std::sync::{Arc, Mutex};

const PAGE_SIZE: usize = 65536;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Value {
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Int {
    I32(i32),
    I64(i64),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Float {
    F32(f32),
    F64(f64),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Trap;

#[derive(Default)]
pub struct Store {
    pub(crate) funcs: Vec<FuncInst>,
    pub(crate) tables: Vec<TableInst>,
    pub(crate) mems: Vec<MemInst>,
    pub(crate) globals: Vec<GlobalInst>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct FuncAddr(pub usize);
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct TableAddr(pub usize);
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct MemAddr(pub usize);
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct GlobalAddr(pub usize);

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ModuleInst {
    pub types: Box<[types::FuncType]>,
    pub func_addrs: Box<[FuncAddr]>,
    pub table_addrs: Box<[TableAddr]>,
    pub mem_addrs: Box<[MemAddr]>,
    pub global_addrs: Box<[GlobalAddr]>,
    pub exports: Box<[ExportInst]>,
}

pub enum FuncInst {
    Module {
        func_type: types::FuncType,
        module: Arc<ModuleInst>,
        code: Func,
    },
    Host {
        func_type: types::FuncType,
        host_code: HostFunc,
    },
}

pub type HostFunc = Arc<Mutex<dyn FnMut(&[Value], &mut [Value]) -> Result<(), String> + Send>>;

#[derive(Debug, PartialEq, Clone)]
pub struct TableInst {
    pub elem: Box<[Option<FuncAddr>]>,
    pub max: Option<u32>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MemInst {
    pub data: Vec<u8>,
    pub max: Option<u32>,
}
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct GlobalInst {
    pub value: Value,
    pub mutable: types::Mut,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ExportInst {
    pub name: String,
    pub value: ExternVal,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ExternVal {
    Func(FuncAddr),
    Table(TableAddr),
    Mem(MemAddr),
    Global(GlobalAddr),
}

impl Store {
    pub(crate) fn funcs(&self, addr: FuncAddr) -> Option<&FuncInst> {
        self.funcs.get(addr.0)
    }

    pub(crate) fn table(&self, addr: TableAddr) -> &TableInst {
        &self.tables[addr.0]
    }

    pub(crate) fn tables_mut(&mut self, addr: TableAddr) -> Option<&mut TableInst> {
        self.tables.get_mut(addr.0)
    }

    pub(crate) fn mem(&self, addr: MemAddr) -> &MemInst {
        &self.mems[addr.0]
    }

    pub(crate) fn mems_mut(&mut self, addr: MemAddr) -> Option<&mut MemInst> {
        self.mems.get_mut(addr.0)
    }

    pub(crate) fn global(&self, addr: GlobalAddr) -> GlobalInst {
        self.globals[addr.0]
    }

    pub(crate) fn set_global(&mut self, addr: GlobalAddr, value: Value) {
        let global = &mut self.globals[addr.0];
        assert!(global.mutable == types::Mut::Var, "global is not mutable");
        global.value = value;
    }
}

impl ModuleInst {
    pub fn types(&self, index: TypeIdx) -> Option<&types::FuncType> {
        self.types.get(index.0)
    }

    pub fn export(&self, name: &str) -> Option<ExternVal> {
        Some(
            self.exports
                .iter()
                .find(|export| export.name == name)?
                .value,
        )
    }

    pub(crate) fn func_addr(&self, index: FuncIdx) -> FuncAddr {
        self.func_addrs[index.0]
    }
}

impl FuncInst {
    pub fn types(&self) -> &types::FuncType {
        match self {
            FuncInst::Module { func_type, .. } => &func_type,
            FuncInst::Host { func_type, .. } => &func_type,
        }
    }

    pub fn locals(&self) -> &[types::ValType] {
        match self {
            FuncInst::Module { code, .. } => &code.locals,
            FuncInst::Host { .. } => &[],
        }
    }
}

impl TableInst {
    pub fn types(&self) -> types::TableType {
        types::TableType(
            types::Limits {
                min: self.elem.len() as u32,
                max: self.max,
            },
            types::ElemType::FuncRef,
        )
    }
}

impl MemInst {
    pub fn types(&self) -> types::MemType {
        types::MemType(types::Limits {
            min: (self.data.len() / PAGE_SIZE) as u32,
            max: self.max,
        })
    }
}

impl GlobalInst {
    pub fn types(&self) -> types::GlobalType {
        types::GlobalType(self.mutable, self.value.to_type())
    }
}

pub(crate) fn funcs<'a>(extern_vals: impl IntoIterator<Item = &'a ExternVal>) -> Vec<FuncAddr> {
    extern_vals
        .into_iter()
        .filter_map(ExternVal::to_func_addr)
        .collect::<Vec<_>>()
}

pub(crate) fn tables<'a>(extern_vals: impl IntoIterator<Item = &'a ExternVal>) -> Vec<TableAddr> {
    extern_vals
        .into_iter()
        .filter_map(ExternVal::to_table_addr)
        .collect::<Vec<_>>()
}

pub(crate) fn mems<'a>(extern_vals: impl IntoIterator<Item = &'a ExternVal>) -> Vec<MemAddr> {
    extern_vals
        .into_iter()
        .filter_map(ExternVal::to_mem_addr)
        .collect::<Vec<_>>()
}

pub(crate) fn globals<'a>(extern_vals: impl IntoIterator<Item = &'a ExternVal>) -> Vec<GlobalAddr> {
    extern_vals
        .into_iter()
        .filter_map(ExternVal::to_global_addr)
        .collect::<Vec<_>>()
}

impl ExternVal {
    pub(crate) fn to_func_addr(&self) -> Option<FuncAddr> {
        match self {
            ExternVal::Func(t) => Some(*t),
            _ => None,
        }
    }

    pub(crate) fn to_table_addr(&self) -> Option<TableAddr> {
        match self {
            ExternVal::Table(t) => Some(*t),
            _ => None,
        }
    }

    pub(crate) fn to_mem_addr(&self) -> Option<MemAddr> {
        match self {
            ExternVal::Mem(t) => Some(*t),
            _ => None,
        }
    }

    pub(crate) fn to_global_addr(&self) -> Option<GlobalAddr> {
        match self {
            ExternVal::Global(t) => Some(*t),
            _ => None,
        }
    }
}
