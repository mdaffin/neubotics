(module
  (type (;0;) (func (param i32) (result i32)))
  (type (;1;) (func (param f32)))
  (type (;2;) (func))
  (import "env" "switch" (func $switch (type 0)))
  (import "env" "ldr" (func $ldr (type 0)))
  (import "env" "set_linvel" (func $set_linvel (type 1)))
  (import "env" "set_angvel" (func $set_angvel (type 1)))
  (func $main (type 2)
    (local i32)
    loop  ;; label = @1
      block  ;; label = @2
        i32.const 0
        call $switch
        br_if 0 (;@2;)
        block  ;; label = @3
          i32.const 0
          call $ldr
          br_if 0 (;@3;)
          i32.const 3
          call $ldr
          local.set 0
          f32.const 0x1.9p+7 (;=200;)
          call $set_linvel
          f32.const -0x1.4p+4 (;=-20;)
          f32.const 0x0p+0 (;=0;)
          local.get 0
          select
          call $set_angvel
          br 2 (;@1;)
        end
        f32.const 0x1.9p+7 (;=200;)
        call $set_linvel
        f32.const 0x1.4p+4 (;=20;)
        call $set_angvel
        br 1 (;@1;)
      end
      f32.const 0x0p+0 (;=0;)
      call $set_linvel
      f32.const 0x0p+0 (;=0;)
      call $set_angvel
      br 0 (;@1;)
    end)
  (table (;0;) 1 1 funcref)
  (memory (;0;) 16)
  (global (;0;) (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1048576))
  (global (;2;) i32 (i32.const 1048576))
  (export "memory" (memory 0))
  (export "main" (func $main))
  (export "__data_end" (global 1))
  (export "__heap_base" (global 2)))
