#![no_main]

extern "C" {
    fn ldr(i: usize) -> bool;
    fn switch(i: usize) -> bool;
    fn set_linvel(v: f32);
    fn set_angvel(v: f32);
}

#[no_mangle]
pub fn main() {
    loop {
        if unsafe { switch(0) } {
            unsafe { set_linvel(0.0) };
            unsafe { set_angvel(0.0) };
        } else if unsafe { ldr(0) } {
            unsafe { set_linvel(200.0) };
            unsafe { set_angvel(1.0) };
        } else if unsafe { ldr(3) } {
            unsafe { set_linvel(200.0) };
            unsafe { set_angvel(-1.0) };
        } else {
            unsafe { set_linvel(200.0) };
            unsafe { set_angvel(0.0) };
        }
    }
}
