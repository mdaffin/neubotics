use anyhow::Result;
use bevy::{
    asset::{AssetLoader, LoadContext, LoadedAsset},
    prelude::*,
    reflect::TypeUuid,
    utils::BoxedFuture,
};

pub struct ModuleHandle {
    pub name: String,
    pub handle: Handle<Module>,
    pub has_loaded: bool,
}

#[derive(Default)]
pub struct ModuleHandles {
    pub modules: Vec<ModuleHandle>,
}

impl ModuleHandles {
    pub fn add_module(&mut self, name: impl Into<String>, module_handle: Handle<Module>) {
        self.modules.push(ModuleHandle {
            name: name.into(),
            handle: module_handle,
            has_loaded: false,
        });
    }

    pub fn get_next_unloaded(&mut self) -> Option<&mut ModuleHandle> {
        self.modules.iter_mut().find(|m| !m.has_loaded)
    }

    pub fn reset(&mut self) {
        for module in self.modules.iter_mut() {
            module.has_loaded = false;
        }
    }
}

pub struct CpuPlugin;

impl Plugin for CpuPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_asset::<Module>()
            .init_asset_loader::<ModuleLoader>();
    }
}

#[derive(Clone, Default)]
pub struct ModuleLoader;

#[derive(Debug, Clone, TypeUuid)]
#[uuid = "d4bd6b78-108f-4644-8265-d8b894984127"]
pub struct Module(pub wasm::Module);

impl AssetLoader for ModuleLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<()>> {
        Box::pin(async move {
            let mut bytes = bytes;
            let module = wasm::Module::from_reader(&mut bytes)?;
            load_context.set_default_asset(LoadedAsset::new(Module(module)));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["wasm"]
    }
}
