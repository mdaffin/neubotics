use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::rapier::geometry::{ColliderBuilder, InteractionGroups};

#[derive(Debug, Default)]
pub struct Switch(pub super::SensorCollisionSet);

#[derive(Debug)]
pub struct Led {
    on_color: Handle<ColorMaterial>,
    off_color: Handle<ColorMaterial>,
}

pub fn spawn(
    parent: &mut ChildBuilder,
    x: f32,
    y: f32,
    on_color: Handle<ColorMaterial>,
    off_color: Handle<ColorMaterial>,
    meshes: &mut ResMut<Assets<Mesh>>,
    collision_group: InteractionGroups,
) {
    let width = 10.0;
    let height = 20.0;
    let range = 10.0;
    let entity = parent
        .spawn(primitive(
            off_color.clone(),
            meshes,
            ShapeType::RoundedRectangle {
                width,
                height,
                border_radius: 5.0,
            },
            TessellationMode::Fill(&FillOptions::default()),
            Vec3::new(x - width / 2.0, y - height / 2.0, 0.4),
        ))
        .with(Switch::default())
        .with(Led {
            on_color,
            off_color,
        })
        .current_entity()
        .unwrap();

    parent.spawn((ColliderBuilder::cuboid(range, 55.0)
        .density(1.0)
        .translation(x + range, y)
        .sensor(true)
        .user_data(entity.to_bits() as u128)
        .collision_groups(collision_group),));
}

pub fn color(
    mut sensor_query: Query<(&Switch, &Led, &mut Handle<ColorMaterial>), Changed<Switch>>,
) {
    for (sensor, led, mut material) in sensor_query.iter_mut() {
        *material = if sensor.0.is_colliding() {
            led.on_color.clone()
        } else {
            led.off_color.clone()
        };
    }
}
