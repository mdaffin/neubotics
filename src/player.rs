use crate::wasm_cpu::Module;
use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::{
    physics::RigidBodyHandleComponent,
    rapier::{
        dynamics::{RigidBodyBuilder, RigidBodySet},
        geometry::{ColliderBuilder, InteractionGroups},
        math::Vector,
    },
};
use std::sync::{Arc, Mutex};

use crate::sensors::{ldr, switch};

#[derive(Debug)]
pub struct Player;

#[derive(Debug, Clone)]
pub struct Velocity(Arc<Mutex<(f32, f32)>>);

#[derive(Debug, Clone)]
pub struct LDRSensors(Arc<Mutex<Vec<bool>>>);

#[derive(Debug, Clone)]
pub struct SwitchSensors(Arc<Mutex<Vec<bool>>>);

pub fn spawn_car1(
    commands: &mut Commands,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    meshes: &mut ResMut<Assets<Mesh>>,
    player_collision_group: InteractionGroups,
    ldr_collision_group: InteractionGroups,
    switch_collision_group: InteractionGroups,
    asset_server: Res<AssetServer>,
) {
    let on_color = materials.add(Color::rgba(1.0, 0.0, 0.0, 1.0).into());
    let off_color = materials.add(Color::rgba(0.6, 0.0, 0.0, 0.2).into());

    let module: Handle<Module> = asset_server.load("car1.wasm");

    commands
        .spawn(SpriteBundle {
            material: materials.add(Color::rgba(0.0, 0.0, 0.0, 0.0).into()),
            sprite: Sprite::new(Vec2::new(200.0, 100.0)),
            ..Default::default()
        })
        .with(RigidBodyBuilder::new_dynamic().translation(-400.0, 0.0))
        .with(Player)
        .with(wasm::Registry::default())
        .with(wasm::Interpreter::default())
        .with(module)
        .with(Velocity(Arc::new(Mutex::new((0.0f32, 0.0f32)))))
        .with(LDRSensors(Arc::new(Mutex::new(Vec::new()))))
        .with(SwitchSensors(Arc::new(Mutex::new(Vec::new()))))
        .with_children(|parent| {
            parent.spawn((ColliderBuilder::cuboid(100.0, 50.0)
                .density(1.0)
                .collision_groups(player_collision_group),));
            parent.spawn(primitive(
                materials.add(Color::rgb(0.3, 0.5, 0.2).into()),
                meshes,
                ShapeType::RoundedRectangle {
                    width: 200.0,
                    height: 100.0,
                    border_radius: 15.0,
                },
                TessellationMode::Fill(&FillOptions::default()),
                Vec3::new(-200.0 / 2.0, -100.0 / 2.0, 0.1),
            ));

            ldr::spawn(
                parent,
                95.0,
                45.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                20.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                -20.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );
            ldr::spawn(
                parent,
                95.0,
                -45.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                ldr_collision_group,
            );

            switch::spawn(
                parent,
                95.0,
                0.0,
                on_color.clone(),
                off_color.clone(),
                meshes,
                switch_collision_group,
            )
        });
}

pub fn run_interpreters(
    module_assets: Res<Assets<Module>>,
    mut query: Query<(
        &Handle<Module>,
        &mut wasm::Registry,
        &mut wasm::Interpreter,
        &Velocity,
        &LDRSensors,
        &SwitchSensors,
    )>,
) {
    for (module_handle, mut registry, mut interpreter, velocity, ldr_sensors, switch_sensors) in
        query.iter_mut()
    {
        if let Some(module) = module_assets.get(module_handle) {
            let module = registry.get("main").unwrap_or_else(|| {
                let set_linvel_addr = wasm::ExternVal::Func(
                    interpreter.alloc_func(create_set_linvel(velocity.clone())),
                );
                let set_angvel_addr = wasm::ExternVal::Func(
                    interpreter.alloc_func(create_set_angvel(velocity.clone())),
                );
                let ldr =
                    wasm::ExternVal::Func(interpreter.alloc_func(create_ldr(ldr_sensors.clone())));
                let switch = wasm::ExternVal::Func(
                    interpreter.alloc_func(create_switch(switch_sensors.clone())),
                );

                let module = interpreter
                    .load_module(&module.0, &[switch, ldr, set_linvel_addr, set_angvel_addr]);
                registry.add_module("main", module.clone());
                module
            });

            if !interpreter.is_running() {
                interpreter.call(module, "main", &[]).unwrap();
            }
            interpreter.advance(100);
        }
    }
}

fn create_set_linvel(velocity: Velocity) -> wasm::FuncInst {
    wasm::FuncInst::Host {
        func_type: wasm::types::FuncType {
            params: wasm::types::ResultType(vec![wasm::types::ValType::F32].into_boxed_slice()),
            results: wasm::types::ResultType(Vec::new().into_boxed_slice()),
        },
        host_code: Arc::new(Mutex::new(
            move |params: &[wasm::Value], _return: &mut [wasm::Value]| -> Result<(), String> {
                match params {
                    [wasm::Value::F32(v)] => velocity.0.lock().unwrap().0 = *v,
                    _ => return Err("value miss match".to_string()),
                }
                Ok(())
            },
        )),
    }
}

fn create_set_angvel(velocity: Velocity) -> wasm::FuncInst {
    wasm::FuncInst::Host {
        func_type: wasm::types::FuncType {
            params: wasm::types::ResultType(vec![wasm::types::ValType::F32].into_boxed_slice()),
            results: wasm::types::ResultType(Vec::new().into_boxed_slice()),
        },
        host_code: Arc::new(Mutex::new(
            move |params: &[wasm::Value], _return: &mut [wasm::Value]| -> Result<(), String> {
                match params {
                    [wasm::Value::F32(v)] => velocity.0.lock().unwrap().1 = *v,
                    _ => return Err("value miss match".to_string()),
                }
                Ok(())
            },
        )),
    }
}

fn create_ldr(sensors: LDRSensors) -> wasm::FuncInst {
    wasm::FuncInst::Host {
        func_type: wasm::types::FuncType {
            params: wasm::types::ResultType(vec![wasm::types::ValType::I32].into_boxed_slice()),
            results: wasm::types::ResultType(vec![wasm::types::ValType::I32].into_boxed_slice()),
        },
        host_code: Arc::new(Mutex::new(
            move |params: &[wasm::Value], results: &mut [wasm::Value]| -> Result<(), String> {
                match params {
                    &[wasm::Value::I32(v)] => {
                        results[0] = wasm::Value::I32(dbg!(sensors
                            .0
                            .lock()
                            .unwrap()
                            .get(v as usize)
                            .cloned()
                            .unwrap_or(false)
                            as i32));
                    }
                    _ => return Err("value miss match".to_string()),
                }
                Ok(())
            },
        )),
    }
}

fn create_switch(sensors: SwitchSensors) -> wasm::FuncInst {
    wasm::FuncInst::Host {
        func_type: wasm::types::FuncType {
            params: wasm::types::ResultType(vec![wasm::types::ValType::I32].into_boxed_slice()),
            results: wasm::types::ResultType(vec![wasm::types::ValType::I32].into_boxed_slice()),
        },
        host_code: Arc::new(Mutex::new(
            move |params: &[wasm::Value], results: &mut [wasm::Value]| -> Result<(), String> {
                match params {
                    &[wasm::Value::I32(v)] => {
                        results[0] = wasm::Value::I32(dbg!(sensors
                            .0
                            .lock()
                            .unwrap()
                            .get(v as usize)
                            .cloned()
                            .unwrap_or(false)
                            as i32));
                    }
                    _ => return Err("value miss match".to_string()),
                }
                Ok(())
            },
        )),
    }
}

pub fn movement(
    mut bodies: ResMut<RigidBodySet>,
    mut player_query: Query<(&Player, &Velocity, &RigidBodyHandleComponent)>,
) {
    for (_, velocity, body_handle) in player_query.iter_mut() {
        let body = bodies.get_mut(body_handle.handle()).unwrap();
        let speed = Vector::new(velocity.0.lock().unwrap().0, 0.0);
        let angular_speed = velocity.0.lock().unwrap().1;
        body.set_linvel(body.position().rotation * speed, true);
        body.set_angvel(angular_speed, true);
    }
}

pub fn ldr_sensors(
    mut player_query: Query<(&mut LDRSensors, &Children), With<Player>>,
    mut ldr_query: Query<&ldr::Ldr>,
) {
    for (sensors, children) in player_query.iter_mut() {
        let mut new_sensors: Vec<_> = children
            .iter()
            .filter_map(|child| {
                if let Ok(ldr) = ldr_query.get_mut(*child) {
                    Some(ldr.0.is_colliding())
                } else {
                    None
                }
            })
            .collect();

        let mut old_sensors = sensors.0.lock().unwrap();
        old_sensors.clear();
        old_sensors.append(&mut new_sensors);
    }
}

pub fn switch_sensors(
    mut player_query: Query<(&mut SwitchSensors, &Children), With<Player>>,
    mut switch_query: Query<&switch::Switch>,
) {
    for (sensors, children) in player_query.iter_mut() {
        let mut new_sensors: Vec<_> = children
            .iter()
            .filter_map(|child| {
                if let Ok(switch) = switch_query.get_mut(*child) {
                    Some(switch.0.is_colliding())
                } else {
                    None
                }
            })
            .collect();

        let mut old_sensors = sensors.0.lock().unwrap();
        old_sensors.clear();
        old_sensors.append(&mut new_sensors);
    }
}
